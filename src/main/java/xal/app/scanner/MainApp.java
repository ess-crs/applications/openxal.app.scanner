/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import java.io.File;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import xal.extension.fxapplication.FxApplication;
import static xal.app.scanner.Hdf5Writer.h5LibsWorks;

public class MainApp extends FxApplication {

    private static final Logger LOGGER = Logger.getLogger(MainApp.class.getName());

    @Override
    public void setup(Stage stage) {
        MAIN_SCENE = "/fxml/ScannerScene.fxml";
        setApplicationName("Scanner Application");
        LOGGER.log(Level.WARNING, "Ignoring useDefaultAccelerator: {0} .. ",
                Boolean.getBoolean("useDefaultAccelerator"));
        DOCUMENT = ScannerDocument.initScannerDocument(stage);
        HAS_SEQUENCE = false;

        stage.setOnCloseRequest((WindowEvent event) -> {
            //your code goes here
            if (DOCUMENT.hasChanges() || MainFunctions.getFunctions().isScanOngoing()) {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                if (MainFunctions.getFunctions().isScanOngoing())
                    a.setContentText("Scan in progress, are you sure you want to exit?");
                else if (DOCUMENT.hasChanges())
                    a.setContentText("Document has unsaved changes, are you sure you want to exit?");
                Optional<ButtonType> result = a.showAndWait();
                if(!result.isPresent() || result.get() != ButtonType.OK)
                    event.consume();
            }
        });


    }

    private void exportToCsv() {
        if (((ScannerDocument)DOCUMENT).getSelectedSets().size() == 0)
            return;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save as CSV");
        fileChooser.setInitialFileName("scandata.csv");

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV Files  ( *.csv )", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File selectedFile = fileChooser.showSaveDialog(null);
        ((ScannerDocument)DOCUMENT).exportToCSV(selectedFile);
    }

    @Override
    public void beforeStart(Stage stage) {
        // Add specific menu items..
        SeparatorMenuItem separator = new SeparatorMenuItem();

        final MenuItem exportToCsvMenu = new MenuItem("Export to CSV");
        exportToCsvMenu.setOnAction(e -> exportToCsv());

        final CheckMenuItem useH5Menu = new CheckMenuItem("Use HDF5");
        SimpleBooleanProperty useH5Property = ((ScannerDocument) DOCUMENT).getUsingHDF5Property();
        useH5Menu.setSelected(useH5Property.get());
        useH5Property.bindBidirectional(useH5Menu.selectedProperty());
        if (!h5LibsWorks()) {
            LOGGER.log(Level.INFO, "HDF5 libraries not found, disabling HDF5");
            useH5Menu.setDisable(true);
        }

        Menu fileMenu = MENU_BAR.getMenus().get(0);
        fileMenu.getItems().add(separator);
        fileMenu.getItems().add(useH5Menu);
        fileMenu.getItems().add(exportToCsvMenu);
        MENU_BAR.getMenus().set(0, fileMenu);
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
