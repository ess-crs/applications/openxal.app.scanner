/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptException;

import xal.ca.Channel;
import xal.ca.ChannelTimeRecord;
import xal.ca.Timestamp;
import xal.smf.Accelerator;
import xal.tools.data.DataAdaptor;
import xal.tools.data.FileDataAdaptor;
import xal.tools.xml.XmlDataAdaptor;
import xal.tools.hdf5.Hdf5DataAdaptor;
import xal.extension.fxapplication.XalFxDocument;

/**
 *
 * @author yngvelevinsen
 */
public class ScannerDocument extends XalFxDocument {

    private static ScannerDocument document;
    /**
     * A dictionary of the created datasets..
     *
     * The key is the timestamp when the measurement started
     */
    private Map<String, double[][]> dataSets;
    /**
     * For every measurement, store the channels read for this measurement..
     *
     * This is only for scalars
     *
     * The key is the timestamp when the measurement started
     */
    private Map<String, List<Channel>> allPVrbScalars;
    /**
     * For every measurement, store the channels written to for this measurement..
     *
     * The key is the timestamp when the measurement started
     */
    private Map<String, List<ChannelWrapper>> allPVw;
    /**
     * For every measurement, store time stamps of all read variables here
     *
     * This is only for the scalars
     *
     * The key is the timestamp when the measurement started
     */
    private Map<String, Timestamp[][]> allTimestamps;
    /**
     * Name of each dataset. If key does not exist, key is used as name (same key for all Maps in this class)
     *
     * The key is the timestamp when the measurement started
     */
    private final Map<String, String> dataSetNames;
    /**
     * Description/notes for each dataset. If key doesn't exist, there are no notes.
     *
     * The key is the timestamp when the measurement started
     */
    private Map<String, String> dataSetNotes;

    /**
     * The number of different scans that have been done
     */
    private final SimpleIntegerProperty numberOfScans;

    private double[][] currentMeasurement;
    private Timestamp[][] currentTimestamps;

    // Use this as a trigger to update the GUI so that we can continue a loaded half-finished measurement
    private SimpleBooleanProperty currentMeasurementWasLoaded;

    // The current number of measurement points done
    private int nCombosDone;

    // Holds the Script Engine for constraints evaluation
    private ScriptEngine constraintsEngine;
    private List<String> acceptableEngines = new ArrayList<>(List.of("graal.js", "nashorn", "js"));
    // if false, no suitable eval engine found hence constraints not possible
    public final boolean constraintsAllowed;

    /**
     * To calculate constraints, we need to know the short hand variable name
     * for each variable..
     */
    private ObservableList<String> constraints;

    /**
     * The delay between successive measurements in milliseconds
     */
    private SimpleLongProperty delayBetweenMeasurements;

    /**
     * The number of measurements to take at each location
     */
    private SimpleIntegerProperty numberMeasurementsPerCombo;

    /**
     * If the initial parameter settings should be included as first and last
     * point of the scan (default should be yes)
     */
    private SimpleBooleanProperty includeInitialSettings;

    /**
     * Scan first "normal" direction, and then backwards (sawtooth scan)
     */
    private SimpleBooleanProperty includeReverseScan;

    /**
     * Scan one PV at the time (keeping others at ref value)
     */
    private SimpleBooleanProperty simple1B1;

    // extra options
    private SimpleBooleanProperty returnHome;

    /**
     * A command that is executed between each scan step
     *
     * If the return value is 0, then scan continues
     *
     * If the return value is 42, scan pauses
     *
     * For any other non-zero return value, scan stops.
     * All settings return to initial if scan is configured that way.
     */
    private SimpleStringProperty commandToExecute;

    /**
     * If true, commandToExecute will be executed between each step
     */
    private SimpleBooleanProperty commandActive;

    // The channels that may be scanned or only read
    private ObservableList<ChannelWrapper> pvChannels;

    // The combination of scan points (each double[] is equal to number of writeables)
    private List<double[]> combos;
    // The initial list of PV values when the scan was initiated
    private double[] initialCombo;

    // Save/restore parameters..
    private final String SCANNER_SR;
    private final String CHANNELS_SR;
    private final String MEASUREMENTS_SR;
    private final String ARRAY_SR;
    private final String CONSTRAINTS_SR;
    private final String CURRENTMEAS_SR;
    private final String SETTINGS_SR;
    private final String TIMESTAMPS_SR;
    private final String TITLE_SR;
    private final String ACTIVE_SCAN_SR;
    private final String ACTIVE_READ_SR;
    private final String NAME_SR;

    private final SimpleDateFormat TIMEFORMAT_SR;

    private FileDataAdaptor da;
    private Hdf5DataAdaptor arrayDataAdaptor;
    private SimpleBooleanProperty useHdf5Property;
    private DataAdaptor currentMeasAdaptor;
    private String currentMeasurementKey;
    // List of selected scans, used by CSV export
    private List<String> currentlySelectedSets;

    // Helper classes

    private Hdf5Writer h5Writer;

    private static final Logger LOGGER = Logger.getLogger(ScannerDocument.class.getName());

    // -- Constructors --

    /**
     *  Create a new empty ScanDocument1D
     *
     * @param stage The stage for this application
     */
    private ScannerDocument(Stage stage) {
        super(stage);

        SCANNER_SR = "ScannerData";
        CHANNELS_SR = "Channels";
        MEASUREMENTS_SR = "measurements";
        ARRAY_SR = "arrayData";
        CONSTRAINTS_SR = "constraints";
        CURRENTMEAS_SR = "currentMeasurement";
        SETTINGS_SR = "settings";
        TIMESTAMPS_SR = "timestamps";
        TITLE_SR = "title";
        ACTIVE_SCAN_SR = "active_scan";
        ACTIVE_READ_SR = "active_read";
        NAME_SR = "name";
        TIMEFORMAT_SR = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        useHdf5Property = new SimpleBooleanProperty(false);
        if (h5Writer.h5LibsWorks())
            useHdf5Property.set(true);
        useHdf5Property.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                LOGGER.log(Level.INFO, "HDF5 support enabled");
                h5_updateDatasets();
                if (sourceSetAndValid()) {
                    try {
                        h5Writer.setSource(source.getFile());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                LOGGER.log(Level.INFO, "HDF5 support disabled");
            }
        });

        dataSets = new HashMap<>();
        allPVrbScalars = new HashMap<>();
        allPVw = new HashMap<>();
        allTimestamps = new HashMap<>();
        dataSetNames = new HashMap<>();
        pvChannels = FXCollections.observableArrayList();
        combos = new ArrayList<>();
        constraints = FXCollections.observableArrayList("", "", "", "");
        numberOfScans = new SimpleIntegerProperty(0);
        numberMeasurementsPerCombo = new SimpleIntegerProperty(1);
        delayBetweenMeasurements = new SimpleLongProperty(1500);

        setCurrentMeasurementKey();
        currentMeasurementWasLoaded = new SimpleBooleanProperty(false);

        includeInitialSettings = new SimpleBooleanProperty(true);
        includeReverseScan = new SimpleBooleanProperty(false);
        returnHome = new SimpleBooleanProperty(true);
        simple1B1 = new SimpleBooleanProperty(false);

        commandToExecute = new SimpleStringProperty("");
        commandActive = new SimpleBooleanProperty(false);

        currentlySelectedSets = new ArrayList<>();
        h5Writer = new Hdf5Writer();

        setFileNameExtension();
        HELP_PAGEID="227688413";

        setupConstraintsEngine();
        if (constraintsEngine == null) {
            LOGGER.log(Level.INFO, "No suitable engines found, constraints disabled");
            constraintsAllowed = false;
        } else {
            constraintsAllowed = true;
        }

    }

    public static ScannerDocument getDocument() {
        return document;
    }

    public static ScannerDocument initScannerDocument(Stage stage) {
        document = new ScannerDocument(stage);
        MainFunctions.setMainDocument(document);
        FXMLController.setMainDocument(document);
        return document;
    }

    /**
     * Check what Script Engines are available and set the best option (or null)
     */
    private void setupConstraintsEngine() {
        ScriptEngineManager manager = new ScriptEngineManager();
        List<ScriptEngineFactory> factories = manager.getEngineFactories();
        constraintsEngine = null;
        for (ScriptEngineFactory factory : factories) {
            LOGGER.log(Level.FINEST, "Found script engine {0}", factory.getEngineName());
            for (String engineName: factory.getNames()) {
                if (acceptableEngines.contains(engineName)) {
                    LOGGER.log(Level.FINER, "Using script engine {0} for constraints", factory.getEngineName());
                    constraintsEngine = manager.getEngineByName(engineName);
                    break;
                }
            }
        }
    }

    // -- Getters/setters --

    public double[][] getCurrentMeasurementArray() {
        return currentMeasurement;
    }

    public Timestamp[][] getCurrentTimestampsArray() {
        return currentTimestamps;
    }

    public void resetCurrentMeasArrays() {
        // set the array for current data points
        int x = 2+(getNumberOfCombos() - 2) * numberMeasurementsPerCombo.get();
        int y = ((int) getActivePvWritebacks().count())+((int) getActivePVreadableScalars().count());
        currentMeasurement = new double[x][y];

        // set the array for the current timestamps
        y = (int) getActivePVreadableScalars().count();
        currentTimestamps = new Timestamp[x][y];
    }

    public void increaseNumCombosDone() {
        nCombosDone++;
    }

    public void setZeroCombosDone() {
        nCombosDone = 0;
    }

    public int getNumCombosDone() {
        return nCombosDone;
    }

    public ObservableList<String> getConstraints() {
        return constraints;
    }

    public void setConstraint(int index, String constraint) {
        constraints.set(index, constraint);
    }

    public void clearAllConstraints() {
        for(int i = 0; i < constraints.size(); i++)
            constraints.set(i, "");
    }

    public void setDelayBetweenMeasurements(long delay) {
        delayBetweenMeasurements.set(delay);
    }

    public void setDelayBetweenMeasurements(double delay) {
        delayBetweenMeasurements.set((long) delay);
    }

    public long getDelayBetweenMeasurements() {
        return delayBetweenMeasurements.getValue();
    }

    public int getNumberMeasurementsPerCombo() {
        return numberMeasurementsPerCombo.getValue();
    }

    public void setNumberMeasurementsPerCombo(int numberMeasurements) {
        numberMeasurementsPerCombo.set(numberMeasurements);
    }

    public String getCommandToExecute() {
        return commandToExecute.getValue();
    }

    public void setCommandToExecute(String command) {
        commandToExecute.set(command);
    }

    public ChannelWrapper getPVChannel(int index) {
        return pvChannels.get(index);
    }

    public ObservableList<ChannelWrapper> getPVChannels() {
        return pvChannels;
    }

    public int getNumPVChannels() {
        return pvChannels.size();
    }

    public void addPVChannel(ChannelWrapper channel) {
        if (!pvChannels.contains(channel)) {
            LOGGER.log(Level.FINEST, "Adding channel {0} to channel list", new Object[]{channel.getChannelName()});
            if (channel.hasNode()) {
                if (channel.getIsScannable())
                    channel.isScannedProperty().set(true);
                else
                    channel.isReadProperty().set(true);
            }
            pvChannels.add(channel);
        } else
            LOGGER.log(Level.INFO, "Channel {0} has already been added", channel.getChannelName());
    }

    public Map<String, double[][]> getDataSets() {
        return dataSets;
    }

    public double[][] getDataSet(String setKey) {
        return dataSets.get(setKey);
    }

    /**
     * Get the array of data values for the PV in the specified
     * data set.
     *
     * @param setKey The key of the data set
     * @param pvName The name of the PV
     * @return An array of double values
     */
    public double[] getDataSetForPV(String setKey, String pvName) {
        double[][] dSet = getDataSet(setKey);
        double[] array = new double[dSet.length];
        List<ChannelWrapper> pvW = allPVw.get(setKey);
        List<Channel> pvR = allPVrbScalars.get(setKey);
        int i = -1;
        for (int j=0; j < pvW.size(); j++) {
            if (pvW.get(j).getChannelName().equals(pvName))
                i = j;
        }
        if (i == -1) {
            for (int j=0; j < pvR.size(); j++) {
                if (pvR.get(j).getId().equals(pvName))
                    i = j + pvW.size();
            }
        }
        for (int j=0;j < dSet.length; j++) {
            array[j] = dSet[j][i];
        }
        return array;
    }

    public void setDataSet(String setName) {
        dataSets.put(setName, currentMeasurement);
        dataSetNames.put(setName, setName);
    }

    public List<Channel> getPVrbScalarData(String setKey) {
        return allPVrbScalars.get(setKey);
    }

    public void setPVreadbackScalarData(String setKey, List<Channel> data) {
        allPVrbScalars.put(setKey, data);
    }


    public void setPVreadbackData(String setKey) {
        setPVreadbackScalarData(setKey, getActivePVreadableScalars().map(cw ->
                cw.getChannel()).collect(Collectors.toList()));
    }

    public List<ChannelWrapper> getPVWriteData(String setKey) {
        return allPVw.get(setKey);
    }

    public void setPVwriteData(String setKey, List<ChannelWrapper> writeChannels) {
        allPVw.put(setKey, writeChannels);
    }

    public void setPVwriteData(String setKey) {
        setPVwriteData(setKey, getActivePvWritebacks().collect(Collectors.toList()));
    }

    public Timestamp[][] getTimestamps(String setKey) {
        return allTimestamps.get(setKey);
    }

    public void setTimestamps(String setKey, Timestamp[][] timestamps) {
        allTimestamps.put(setKey, timestamps);
    }

    public void setTimestamps(String setKey) {
        setTimestamps(setKey, currentTimestamps);
    }

    public SimpleIntegerProperty getScanNumberProperty() {
        return numberOfScans;
    }

    public int increaseScanNumber() {
        numberOfScans.set(numberOfScans.get()+1);
        return numberOfScans.get();
    }

    public int getNumberOfCombos() {
        return combos.size();
    }
    public double[] getCombo(int i) {
        return combos.get(i);
    }

    public void unsetInitialCombo() {
        initialCombo = null;
    }

    public double[] getInitialCombo() {
        return initialCombo;
    }

    public void setCommandActive(boolean active) {
        commandActive.set(active);
    }

    public boolean isCommandActive() {
        return commandActive.get();
    }

    public void setIncludeInitialSettings(boolean include) {
        includeInitialSettings.set(include);
    }

    public boolean getIncludeInitialSettings() {
        return includeInitialSettings.get();
    }

    public SimpleBooleanProperty getIncludeInitialSettingsProperty() {
        return includeInitialSettings;
    }

    public void setIncludeReverseScan(boolean include) {
        includeReverseScan.set(include);

    }

    public boolean getIncludeReverseScan() {
        return includeReverseScan.get();
    }

    public SimpleBooleanProperty getIncludeReverseScanProperty() {
        return includeReverseScan;
    }
    public SimpleBooleanProperty getSimple1B1Property() {
        return simple1B1;
    }


    public void setReturnHome(boolean return_home) {
        returnHome.set(return_home);
    }

    public boolean getReturnHome() {
        return returnHome.get();
    }

    public SimpleBooleanProperty getReturnHomeProperty() {
        return returnHome;
    }

    // -- Listeners --

    public void addCurrentMeasLoadedListener(ChangeListener listener) {
        currentMeasurementWasLoaded.addListener(listener);
    }

    public void addDelayBetweenMeasListener(ChangeListener listener) {
        delayBetweenMeasurements.addListener(listener);
    }

    public void addNumberMeasPerCombosListener(ChangeListener listener) {
        numberMeasurementsPerCombo.addListener(listener);
    }

    // -- File I/O --

    /**
     *  Save the ScannerDocument document to the specified URL.
     *
     *  @param  url  The file URL where the data should be saved
     */
    @Override
    public void saveDocumentAs(URL url) {
        LOGGER.log(Level.FINER, "Saving document, filename {0}", url);
        if (useHdf5Property.get()) {
            try {
                h5Writer.setSource(url.getFile());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        initDocumentAdaptor();
        DataAdaptor scannerAdaptor =  da.childAdaptor(SCANNER_SR);
        currentMeasAdaptor = null;
        scannerAdaptor.setValue(TITLE_SR, url.getFile());
        scannerAdaptor.setValue("date", TIMEFORMAT_SR.format(new Date()));


        // Store the settings..
        DataAdaptor settingsAdaptor = scannerAdaptor.createChild(SETTINGS_SR);
        settingsAdaptor.setValue("MeasurementDelay", delayBetweenMeasurements.get());
        settingsAdaptor.setValue("MeasurementPerCombo", numberMeasurementsPerCombo.get());
        settingsAdaptor.setValue("IncludeInitialSettings", includeInitialSettings.get());
        settingsAdaptor.setValue("IncludeReverseScan", includeReverseScan.get());
        settingsAdaptor.setValue("ReturnHome", returnHome.get());
        settingsAdaptor.setValue("UseHDF5", useHdf5Property.get());

        // Store information about all measurements done..
        if (!useHdf5Property.get()) {
            DataAdaptor measurementsScanner = scannerAdaptor.createChild(MEASUREMENTS_SR);
            dataSets.entrySet().forEach(measurement -> {
                // convenience variables..
                String setKey = measurement.getKey();
                List<ChannelWrapper> pvW = allPVw.get(setKey);
                List<Channel> pvR = allPVrbScalars.get(setKey);
                Timestamp[][] tstamps = allTimestamps.get(setKey);
                DataAdaptor measurementAdaptor = measurementsScanner.createChild("measurement");
                measurementAdaptor.setValue(TITLE_SR, getSetName(setKey));
                for (int i=0;i<measurement.getValue()[0].length;i++) {
                    DataAdaptor channelAdaptor = measurementAdaptor.createChild("channel");
                    if (i<pvW.size()) {
                        channelAdaptor.setValue(NAME_SR, pvW.get(i).getChannelName());
                        channelAdaptor.setValue("type", "w");
                    } else {
                        channelAdaptor.setValue(NAME_SR, pvR.get(i-pvW.size()).getId());
                        channelAdaptor.setValue("type", "r");
                    }
                    double[] data = new double[measurement.getValue().length];
                    // DataAdaptor is not supporting BigDecimals, so for now we convert to doubles
                    double[] tstampArray = new double[measurement.getValue().length];
                    String tstamps_str = "";
                    for (int j = 0;j<measurement.getValue().length;j++) {
                        data[j]=measurement.getValue()[j][i];
                        if (i>=pvW.size()) {
                            if (j!=0)
                                tstamps_str=tstamps_str.concat(", ");
                            tstamps_str=tstamps_str.concat(tstamps[j][i-pvW.size()].getFullSeconds().toString());
                            tstampArray[j] = tstamps[j][i-pvW.size()].getFullSeconds().doubleValue();
                        }
                        channelAdaptor.setValue("data", data);
                    }
                    channelAdaptor.setValue("data", data);
                    if (i>=pvW.size())
                        channelAdaptor.setValue(TIMESTAMPS_SR, tstampArray);
                }
            });
        }

        // Store information about current measurement setup..

        // Store list of variables to read & write.. ChannelWrapper objects
        DataAdaptor scanpvScanner = scannerAdaptor.createChild(CHANNELS_SR);
        pvChannels.forEach( pv -> {
            DataAdaptor scan_PV_name =  scanpvScanner.createChild("PV");
            scan_PV_name.setValue(NAME_SR, pv.getChannelName() );
            scan_PV_name.setValue("min", pv.startProperty().get() );
            scan_PV_name.setValue("max", pv.endProperty().get() );
            scan_PV_name.setValue("npoints", pv.numPointsProperty().get() );
            scan_PV_name.setValue("instance", pv.instanceProperty().get() );
            scan_PV_name.setValue(ACTIVE_SCAN_SR, pv.isScannedProperty().get() );
            scan_PV_name.setValue(ACTIVE_READ_SR, pv.isReadProperty().get() );
            scan_PV_name.setValue("node", pv.getAcceleratorNodeId());
        });

        DataAdaptor constraintsAdaptor = scannerAdaptor.createChild(CONSTRAINTS_SR);
        constraints.forEach( constraint -> {
            if (! constraint.isEmpty())
                constraintsAdaptor.createChild("constraint").setValue("value", constraint);
            });

        try {
            da.writeToUrl( url );
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.FINEST, "Saved document");
        setHasChanges(false);

    }

    /**
     * TODO This is a bit more cumbersome to complete, so if someone feels encouraged.. enjoy!
     *
     * @param setKey
     * @throws Exception
     */
    private void h5_readKey(String setKey) throws Exception {
        // for (String pvName : h5GetAllGroups(source.getFile(), setKey)) {
            // h5GetMeasurementSet(setKey + "/" + pvName);
        // }
        numberOfScans.set(numberOfScans.get()+1);
    }

    /**
     * Write a complete data set to HDF5 file.
     *
     * DO NOT USE FOR WRITING CURRENT MEASUREMENT
     *
     * This is meant to be used when changing to HDF5 mode after scans have completed.
     * E.g. if one has loaded measurements from an XML file and then switched to use HDF5.
     *
     * @param setKey The key of the measurement
     */
    private void h5_writeFullDataset(String setKey) throws Exception {
        Timestamp[][] tStamps = getTimestamps(setKey);
        List<ChannelWrapper> pvW = allPVw.get(setKey);
        List<Channel> pvR = allPVrbScalars.get(setKey);
        String channelName;
        String tStamp;
        for (int i=0; i < pvR.size(); i++) {
            channelName = pvR.get(i).getId();
            double[] data = getDataSetForPV(setKey, channelName);
            for (int j=0; j < tStamps.length; j++) {
                tStamp = tStamps[j][i].getFullSeconds().toString();
                h5Writer.writeDoubleAsHDF5DataSet("/" + setKey +
                        "/" + channelName, tStamp, data[j]);
            }
        }
        // For the Write data we somewhat incorrectly use the timestamp of the first readback PV.
        for (int i=0; i < pvW.size(); i++) {
            channelName = pvW.get(i).getChannelName();
            double[] dataS = getDataSetForPV(setKey, channelName);
            for (int j=0; j < tStamps.length; j++) {
                tStamp = tStamps[j][0].getFullSeconds().toString();
                h5Writer.writeDoubleAsHDF5DataSet("/" + setKey +
                        "/" + channelName, tStamp, dataS[j]);
            }
        }
    }

    /**
     * When turning on HDF5 usage, all measurement sets in memory should
     * be written to file. All data sets already existing in file should
     * be uploaded to memory
     *
     */
    private void h5_updateDatasets() {
        try {
            ArrayList<String> h5Sets = h5Writer.h5GetAllGroups("/");

            // Write sets in memory to HDF5 file
            for (String dKey : dataSets.keySet()) {
                if (!h5Sets.contains(dKey)) {
                    h5_writeFullDataset(dKey);
                }
            }
            // Load sets in HDF5 file to memory (not yet finished)
            for (String dKey : h5Sets) {
                if (!dataSets.containsKey(dKey)) {
                    h5_readKey(dKey);
                }
            }
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    private static Timestamp[] getColumn(Timestamp[][] array, int index){
        Timestamp[] column = new Timestamp[array.length];
        for(int i=0; i<column.length; i++){
           column[i] = array[i][index];
        }
        return column;
    }

    private List<String> getHeaders(String setKey) {
        List<ChannelWrapper> pvW = allPVw.get(setKey);
        List<Channel> pvR = allPVrbScalars.get(setKey);
        List<String> headers = new ArrayList<>();
        pvW.forEach((p) -> headers.add(p.getChannelName()));
        pvR.forEach((p) -> headers.add(p.getId()));

        return headers;
    }


    /**
     * Called when there is a change in selection in the list of completed
     * scans
     * @param selectedItems list of items selected by user
     */
    public void setSelectedSets(List<String> selectedItems) {
        currentlySelectedSets.clear();
        selectedItems.stream().forEach(si -> currentlySelectedSets.add(si));
    }
    public List<String> getSelectedSets() {
        return currentlySelectedSets;
    }

    public void deleteSet(String setName) {
        String setKey = getSetKey(setName);
        dataSetNames.remove(setKey);
        dataSets.remove(setKey);
        allPVrbScalars.remove(setKey);
        allPVw.remove(setKey);
        allTimestamps.remove(setKey);
    }

    /**
     * TODO combine multiple sets, for now just dumps the first selected set
     * (in case of multiple selection)
     * @param filePath Path to new CSV file
     */
    public void exportToCSV(File filePath) {
        String setKey = currentlySelectedSets.get(0);
        Timestamp[] ts = getColumn(allTimestamps.get(setKey), 0);
        List<String> headers = getHeaders(setKey);
        LOGGER.log(Level.INFO, "Exporting set {0} to {1}", new Object[]{setKey, filePath});
        Csv csvWriter = new Csv(filePath);
        csvWriter.exportToCSV(headers, ts, dataSets.get(setKey));
    }
    
    private void setFileNameExtension() {
        FILETYPE_DESCRIPTION = "XML scan file";
        DEFAULT_FILENAME="Data.scan.xml";
        WILDCARD_FILE_EXTENSION = "*.scan.xml";
    }

    public boolean getUsingHDF5() {
        return useHdf5Property.get();
    }

    public SimpleBooleanProperty getUsingHDF5Property() {
        return useHdf5Property;
    }


    public void saveCurrentMeas(int nmeas) {
        if (useHdf5Property.get()) // This is only for XML storing..
            return;
        if (da == null)
            initDocumentAdaptor();
        if (currentMeasAdaptor == null) {
            currentMeasAdaptor=da.childAdaptor(SCANNER_SR).createChild(CURRENTMEAS_SR);
        }

        String tstamps_str = "";
        for(Timestamp tstamp : currentTimestamps[nmeas]) {
            if (!"".equals(tstamps_str))
                tstamps_str=tstamps_str.concat(", ");
            tstamps_str=tstamps_str.concat(tstamp.getFullSeconds().toString());
        }
        DataAdaptor stepAdaptor = currentMeasAdaptor.createChild("step");
        stepAdaptor.setValue("values", currentMeasurement[nmeas]);
        stepAdaptor.setValue(TIMESTAMPS_SR, tstamps_str);

        if (source!=null)
            try {
                da.writeToUrl( source );
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }


    public void setCurrentMeasurementKey(String name) {
        currentMeasurementKey = name;
    }

    public void setCurrentMeasurementKey() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date());
        ScannerDocument.this.setCurrentMeasurementKey(timeStamp);
    }

    public String getCurrentMeasurementKey() {
        return currentMeasurementKey;
    }

    /**
     * Get the name for this data set
     * if no name has been defined, key is the name
     * @param key unique key for this data set
     * @return name for this data set
     */
    public String getSetName(String key) {
        return dataSetNames.containsKey(key) ? dataSetNames.get(key) : key;
    }

    /**
     * Get the key for a named measurement (data set)
     *
     * @param setName Name of measurement
     * @return The key for the given measurement name
     */
    public String getSetKey(String setName) {
        if (dataSetNames.containsKey(setName))
            return setName;
        for (Map.Entry<String,String> entry : dataSetNames.entrySet()) {
                if (setName.equals(entry.getValue())) return entry.getKey();
            }
        return setName;
    }


    /**
    * Set name of a measurement. Name could also be
    * considered the short description of the measurement
    *
    * @param oldName Current name of the data set
    * @param newName New name of the data set
    */
    public void setMeasurementName(String oldName, String newName) throws NoSuchFieldException {
        String key = getSetName(oldName);
        if (! dataSetNames.containsKey(key))
            throw new NoSuchFieldException("No data set named "+ key);
        if (key.equals(newName) || dataSetNames.get(key).equals(newName)) return;
        dataSetNames.put(key, newName);
        if (source != null && useHdf5Property.get()) {
            try {
                // Write name as attribute to data set
                h5Writer.writeHDF5Attribute("/" + key, "title", newName);
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Set the description of a measurement. Old description is overwritten.
     *
     * @param hashKey The hash of the measurement data set
     * @param notes New notes for this data set
     */
    public void setDataSetNotes(String hashKey, String notes) {
        dataSetNotes.put(hashKey, notes);
    }


    /**
     * This will write the array to the currently active measurement file
     *
     * @param arrayReadings
     */
    public int h5_writeCurrentArrayData(ArrayList<ChannelTimeRecord> arrayReadings) {
        LOGGER.log(Level.FINER, "Writing array data started");
        if (source == null || ! useHdf5Property.get()) {
            LOGGER.log(Level.WARNING,
                    "Array data selected but no HDF5 file set, data discarded");
            return 1;
        }

        for (int i = 0;i<arrayReadings.size();i++) {
            ChannelTimeRecord timeRecord = arrayReadings.get(i);
            ChannelWrapper channel = getActivePVreadableArray(i);
            String tStamp = timeRecord.getTimestamp().getFullSeconds().toString();
            try {
                h5Writer.writeArrayAsHDF5DataSet("/" + currentMeasurementKey +
                        "/" + channel.getChannelName(), tStamp, timeRecord.doubleArray());
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        LOGGER.log(Level.FINER, "Writing array data finished");
        return 0;
    }

    public int h5_writeCurrentScalarData(ArrayList<ChannelTimeRecord> scalarReadings) {
        if (source == null || ! useHdf5Property.get())
            return 1;
        LOGGER.log(Level.FINEST, "Writing scalar data started");
        for (int i = 0;i<scalarReadings.size();i++) {
            ChannelTimeRecord timeRecord = scalarReadings.get(i);
            ChannelWrapper channel = getActivePVreadableScalar(i);
            String tStamp = timeRecord.getTimestamp().getFullSeconds().toString();
            try {
                h5Writer.writeDoubleAsHDF5DataSet("/" + currentMeasurementKey +
                        "/" + channel.getChannelName(), tStamp, timeRecord.doubleValue());
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        LOGGER.log(Level.FINER, "Writing scalar data finished");
        return 0;
    }

    public int h5_writeCurrentSetValues() {
        if (source == null || ! useHdf5Property.get())
            return 1;
        double[] combo = combos.get(nCombosDone);
        for (int i = 0;i<combo.length; i++) {
            try {
                ChannelTimeRecord timeRecord = getActivePVwriteback(i).getChannel().getTimeRecord();
                h5Writer.writeDoubleAsHDF5DataSet(
                        "/" + currentMeasurementKey + "/" + getActivePVwriteback(i).getChannelName(),
                        timeRecord.getTimestamp().getFullSeconds().toString(),
                        timeRecord.doubleValue());
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, "Failed to store set value {0}/{1}, error {2}", new Object[]{currentMeasurementKey, getActivePVwriteback(i).getChannelName(), ex.getMessage()});
            }
        }
        return 0;
    }

    /**
     *  Reads the content of the document from the specified URL, and loads the information into the application.
     *
     * @param  url  The path to the XML file (ignored, see setSource)
     */
    @Override
    public void loadDocument(URL url) {
        loadDocument(false);
    }

    /**
     * @param testMode if true, skip connecting to channels etc
     */
    public void loadDocument(boolean testMode) {
        DataAdaptor scannerAdaptor =  XmlDataAdaptor.adaptorForUrl( source, false ).childAdaptor(SCANNER_SR);
        if (useHdf5Property.get()) {
            try {
                h5Writer.setSource(source.getFile());
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }

        Accelerator acc = Model.getInstance().getAccelerator();

        // Load the settings
        DataAdaptor settingsAdaptor = scannerAdaptor.childAdaptor(SETTINGS_SR);
        delayBetweenMeasurements.set(settingsAdaptor.longValue("MeasurementDelay"));
        numberMeasurementsPerCombo.set(settingsAdaptor.intValue("MeasurementPerCombo"));
        includeInitialSettings.set(settingsAdaptor.booleanValue("IncludeInitialSettings"));
        includeReverseScan.set(settingsAdaptor.booleanValue("IncludeReverseScan"));
        returnHome.set(settingsAdaptor.booleanValue("ReturnHome"));
        useHdf5Property.set(settingsAdaptor.booleanValue("UseHDF5"));

        // Load list of variables to read & write.. ChannelWrapper objects
        DataAdaptor channelScanner = scannerAdaptor.childAdaptor(CHANNELS_SR);
        pvChannels.clear();
        channelScanner.childAdaptors().forEach(new Consumer<DataAdaptor>() {
            @Override
            public void accept(DataAdaptor childAdaptor) {
                String name = childAdaptor.stringValue(NAME_SR);
                String nodeId = childAdaptor.stringValue("node");
                boolean active_scan = childAdaptor.booleanValue(ACTIVE_SCAN_SR);
                boolean active_read = childAdaptor.booleanValue(ACTIVE_READ_SR);
                LOGGER.log(Level.FINER, "Loading PV {0}, scan: {1}, read: {2}",
                        new Object[]{name, active_scan, active_read});

                double start = childAdaptor.doubleValue("min");
                double end = childAdaptor.doubleValue("max");
                int npoints = childAdaptor.intValue("npoints");
                String instance = childAdaptor.stringValue("instance");

                Channel chan = acc.channelSuite().getChannelFactory().getChannel(name);

                ChannelWrapper cWrap;
                if (testMode)
                    cWrap = new ChannelWrapper(chan, false);
                else if (nodeId == null)
                    cWrap = new ChannelWrapper(chan);
                else
                    cWrap = new ChannelWrapper(chan, acc.getNode(nodeId), true);
                cWrap.isScannedProperty().set(active_scan);
                cWrap.isReadProperty().set(active_read);
                cWrap.startProperty().set(start);
                cWrap.endProperty().set(end);
                cWrap.numPointsProperty().set(npoints);
                // if instance equals x0 it means the variable was never used.
                if (!instance.equals("x0"))
                    cWrap.forceInstance(instance);

                pvChannels.add(cWrap);
            }
        });

        // Load all constraints from the file
        DataAdaptor constraintsAdaptor = scannerAdaptor.childAdaptor(CONSTRAINTS_SR);
        for(int i = 0; i<constraints.size();i++)
            constraints.set(i, "");
        for (int i = 0; i<constraintsAdaptor.childAdaptors().size();i++) {
            DataAdaptor childAdaptor = constraintsAdaptor.childAdaptors().get(i);
            constraints.set(i,childAdaptor.stringValue("value"));
        }

        // Load earlier measurements..
        if ( scannerAdaptor.childAdaptor(MEASUREMENTS_SR) != null) {
            DataAdaptor measurementsScanner = scannerAdaptor.childAdaptor(MEASUREMENTS_SR);
            measurementsScanner.childAdaptors().forEach(measAdaptor -> {
                LOGGER.log(Level.FINEST, "Loading measurement {0}",
                        measAdaptor.stringValue(TITLE_SR));
                List<ChannelWrapper> pvW = new ArrayList<>();
                List<Channel> pvR = new ArrayList<>();

                int numCombos = measAdaptor.childAdaptors().get(0).doubleArray("data").length;
                int numChannels = measAdaptor.childAdaptors().size();
                int numReadChannels = (int) measAdaptor.childAdaptors().stream().filter( childAdaptor ->
                        ("r".equals(childAdaptor.stringValue("type"))) ).count();

                double[][] data = new double[numCombos][numChannels];
                Timestamp[][] tstamps = new Timestamp[numCombos][numReadChannels];
                int iReadChan = 0;
                for (int ichan=0;ichan<numChannels;ichan++) {
                    DataAdaptor chanAdaptor = measAdaptor.childAdaptors().get(ichan);
                    boolean isRead = "r".equals(chanAdaptor.stringValue("type"));
                    boolean isWrite = "w".equals(chanAdaptor.stringValue("type"));
                    double[] channelData = chanAdaptor.doubleArray("data");
                    for (int icombo=0;icombo<numCombos;icombo++) {
                        data[icombo][ichan] = channelData[icombo];
                    }
                    if (isRead) {
                        String[] tstampData = chanAdaptor.stringValue(TIMESTAMPS_SR).split(", ");
                        for (int icombo=0;icombo<numCombos;icombo++) {
                            tstamps[icombo][iReadChan] = new Timestamp(new BigDecimal(tstampData[icombo]));
                        }
                        iReadChan+=1;
                    }
                    Channel chan = acc.channelSuite().getChannelFactory().getChannel(chanAdaptor.stringValue(NAME_SR));
                    ChannelWrapper cWrap = new ChannelWrapper(chan, false);
                    if (isWrite) {
                        pvW.add(cWrap);
                    } else if (isRead) {
                        pvR.add(chan);
                    }
                }
                dataSetNames.put(measAdaptor.stringValue(TITLE_SR), measAdaptor.stringValue(TITLE_SR));
                dataSets.put(measAdaptor.stringValue(TITLE_SR), data);
                allPVw.put(measAdaptor.stringValue(TITLE_SR), pvW);
                allPVrbScalars.put(measAdaptor.stringValue(TITLE_SR), pvR);
                allTimestamps.put(measAdaptor.stringValue(TITLE_SR), tstamps);
                numberOfScans.set(numberOfScans.get()+1);

            });
        }

        // Update combos calculation
        calculateCombos();

        currentMeasAdaptor = scannerAdaptor.childAdaptor(CURRENTMEAS_SR);
        if ( currentMeasAdaptor != null) {
            nCombosDone=currentMeasAdaptor.childAdaptors().size();
            LOGGER.log(Level.FINEST,
                    "Loading unfinished measurement, found {0} measurement points", nCombosDone);
            int nActiveWrites = (int) getActivePvWritebacks().count();
            int nActiveReads = (int) getActivePVreadableScalars().count();
            int nActiveChannels = nActiveWrites + nActiveReads;
            currentMeasurement = new double[getNumberOfCombos()][nActiveChannels];
            currentTimestamps = new Timestamp[2+(getNumberOfCombos()-2)*numberMeasurementsPerCombo.get()][nActiveReads];
            for(int i = 0;i<nCombosDone;i++) {
                double [] values = currentMeasAdaptor.childAdaptors().get(i).doubleArray("values");
                String[] tstamps = currentMeasAdaptor.childAdaptors().get(i).stringValue(TIMESTAMPS_SR).split(", ");
                for (int j=0;j<nActiveChannels;j++) {
                    currentMeasurement[i][j] = values[j];
                    if (j>= getActivePvWritebacks().count()) {
                        currentTimestamps[i][j-nActiveWrites] = new Timestamp(new BigDecimal(tstamps[j-nActiveWrites]));
                    }
                }
            }
            currentMeasurementWasLoaded.set(true);
            // TODO Is there a way to trigger plot of the current measurement here?
        }
        setHasChanges(false);
    }

    // -- Functions --


    private void initDocumentAdaptor() {
        //if (USE_HDF5)
        //    da = Hdf5DataAdaptor.newEmptyDocumentAdaptor();
        //else
            da = XmlDataAdaptor.newEmptyDocumentAdaptor();
        da.createChild(SCANNER_SR);

    }

    public Stream<ChannelWrapper> getActivePVreadableScalars() {
        return pvChannels.stream().filter( channel -> (channel.getIsRead() && channel.getIsScalar()));
    }

    public Stream<ChannelWrapper> getActivePVreadableArrays() {
        return pvChannels.stream().filter( channel -> (channel.getIsRead() &&  ! channel.getIsScalar()));
    }

    public Stream<ChannelWrapper> getActivePvWritebacks() {
        return pvChannels.stream().filter( channel -> (channel.getIsScanned()));
    }

    /**
     * This function returns the i'th scalar PV to be read
     *
     * @param i the channel index (counting active only)
     * @return Return the i'th active readable channel
     */
    public ChannelWrapper getActivePVreadableScalar(int i) {
        int j=-1;
        for (ChannelWrapper cw : pvChannels) {
            if (cw.getIsRead() && cw.getIsScalar())
                j++;
            if (j==i)
                return cw;
        }
        return null;
    }

    /**
     * This function returns the i'th array PV to be read
     *
     * @param i the channel index (counting active only)
     * @return Return the i'th active readable channel
     */
    public ChannelWrapper getActivePVreadableArray(int i) {
        int j=-1;
        for (ChannelWrapper cw : pvChannels) {
            if (cw.getIsRead() && !cw.getIsScalar())
                j++;
            if (j==i)
                return cw;
        }
        return null;
    }

    /**
     *
     * @param i the channel index (counting active only)
     * @return Return the i'th active scannable channel
     */
    public ChannelWrapper getActivePVwriteback(int i) {
        int j=-1;
        for (ChannelWrapper cw : pvChannels) {
            if (cw.getIsScanned())
                j++;
            if (j==i)
                return cw;
        }
        return null;
    }

    private boolean hasConstraints() {
        return constraints.stream().anyMatch( constraint -> (constraint.trim().length()>0));
    }

    /**
     * Check a combo for any of the potentially defined constraints
     *
     * TODO there are probably better/safer ways to do this?
     *
     * @param combo the combo to verify
     * @return true if combo pass all constraints
     * @throws ScriptException if there was an evaluation issue
     */
    public boolean checkConstraints(double[] combo) throws ScriptException {
        if (constraintsAllowed) {
            for(int i=0;i<combo.length;i++) {
                constraintsEngine.eval(getActivePVwriteback(i).instanceProperty().get()+"="+combo[i]);
            }
            for(String constraint: constraints) {
                if (constraint.trim().length()>0) {
                    if (!(boolean) constraintsEngine.eval(constraint))
                        return false;
                }
            }
        }
        return true;
    }

    // Return the reference value of the i'th ACTIVE pvWriteable
    private double getPvRefValue(int i) {
        int j = -1;
        for (ChannelWrapper cw : pvChannels) {
            if (cw.getIsScanned())
                j++;
            if (i==j) {
                return cw.refProperty().get();
            }
       }
       return Double.NaN;
    }

    public int calculateCombos() {

        LOGGER.log(Level.FINER, "Recalculating steps");

        int nchannels = (int) getActivePvWritebacks().count();

         combos.clear();
         if(currentMeasurementWasLoaded.get()) {
             currentMeasurementWasLoaded.set(false);
         } else {
             nCombosDone = 0;
         }

        // Calculate the initial combo (the settings when precalculate was pushed)
        LOGGER.log(Level.FINER, "Reading initial PV values");
        initialCombo = new double[(int) getActivePvWritebacks().count()];
        for (int i = 0; i<(int) getActivePvWritebacks().count(); i++) {
            initialCombo[i] = getPvRefValue(i);
            LOGGER.log(Level.FINEST,
                    "Initial value for {0} found to be {1}",
                    new Object[]{ getActivePVwriteback(i), initialCombo[i]});
        }

        // Calculate the correct amount of combos..
        int ncombos;
        if (simple1B1.get()) {
            ncombos = 0;
            ncombos = getActivePvWritebacks().map(cw -> cw.getNumPoints()).reduce(ncombos,
                    (accumulator, _item) -> accumulator + _item);
            // Double the combos if reverse scan is included
            if (includeReverseScan.get())
                ncombos = 2 * ncombos - nchannels;
        } else {
            ncombos = 1;
            ncombos = getActivePvWritebacks().map(cw -> cw.getNumPoints()).reduce(ncombos,
                    (accumulator, _item) -> accumulator * _item);
            // Double the combos if reverse scan is included
            if (includeReverseScan.get())
                ncombos = 2 * ncombos - 1;
        }
        // Two extra for initial settings as first and last combo
        if (includeInitialSettings.get()) {
            ncombos+=2;
        }
        // Initialize combos with the initial settings of each PV,
        for (int i = 0;i<ncombos;i++) {
            combos.add(new double[nchannels]);
            for (int j=0;j<nchannels;j++) {
                combos.get(i)[j] = initialCombo[j];
            }
        }

        // Insert all numbers..
        // n1 will say how many times each number should currently be repeated
        int n1 = ncombos;
        if (includeReverseScan.get()) {
            n1 = (n1 + 1) / 2;
            if (includeInitialSettings.get())
                n1 += 1;
        }
        // n2 will say how many times we should loop the current PV
        int n2 = 1;
        // The combo index we are currently inserting
        int m = 0;
        if (includeInitialSettings.get()) m++;
        // Write out one parameter at the time
        for (int i=0; i<nchannels;i++) {
            ChannelWrapper cw = getActivePVwriteback(i);
            if (simple1B1.get()) {
                double sp;
                n2 = cw.getScanPoints().length;
                for (int spi=0; spi < n2; spi++) {
                    sp = cw.getScanPoints()[spi];
                    combos.get(m+spi)[i] = sp;
                    if (includeReverseScan.get()) {
                        combos.get(2 * (n2 -1) + m - spi)[i] = sp;
                    }
                }
                m += n2;
                if (includeReverseScan.get()) m += n2 -1;
            } else {
                // Here follows the normal scan mode
                // reset combo indexer
                m = 0;
                if (includeInitialSettings.get()) m++;
                n1 /= cw.getNumPoints();
                for (int l=0; l<n2; l++) {
                    double[] sPoints;
                    if (l % 2 == 0)
                        sPoints = cw.getScanPoints();
                    else
                        sPoints = cw.getScanPoints(true);
                    for (double sp : sPoints) {
                        for (int k=0; k<n1; k++) {
                            combos.get(m)[i] = sp;
                            if (includeReverseScan.get() && m < ncombos / 2 ) {
                                combos.get(ncombos - m - 1)[i] = sp;
                            }
                            m ++;
                        }
                    }
                }
                n2 *= cw.getNumPoints();
            }
         }


        // Now we check if any of the combos are invalid..
        if (hasConstraints()) {
            int i=0;
            while(i<combos.size())
            {
                try {
                    if (!checkConstraints(combos.get(i))) {
                        combos.remove(i);
                        continue;
                    }
                } catch (ScriptException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
                i+=1;
            }
        }
        return combos.size();
    }

    @Override
    public void newDocument() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
