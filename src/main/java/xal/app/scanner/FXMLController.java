/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import xal.extension.fxapplication.Controller;

public class FXMLController extends Controller {

    @FXML
    private URL location;

    @FXML
    private TableView<ChannelWrapper> pvTable;

    @FXML
    private TableColumn<ChannelWrapper, String> pvNameColumn;

    @FXML
    private TableColumn<ChannelWrapper, Boolean> scanColumnSelect;

    @FXML
    private TableColumn<ChannelWrapper, Boolean> readColumnSelect;

    @FXML
    private Tab tabConfigure;

    @FXML
    private Tab tabSelect;

    @FXML // fx:id="delayBetweenMeas"
    private TextField delayBetweenMeas;

    @FXML // fx:id="measPerPoint"
    private TextField measPerPoint;

    @FXML // fx:id="measDelaySetButton"
    private Button measDelaySetButton;

    @FXML // fx:id="nMeasPerSettingSetButton"
    private Button nMeasPerSettingSetButton;

    @FXML
    private TextField commandToExecute;

    @FXML
    private CheckBox commandActivated;

    @FXML
    private TableView<ChannelWrapper> listOfWriteables;

    @FXML
    private TableColumn<ChannelWrapper, String> listOfWriteablesShortVar;

    @FXML
    private TableColumn<ChannelWrapper, String> listOfWriteablesPV;

    @FXML
    private TableColumn<ChannelWrapper, String> listOfWriteablesUnit;

    @FXML
    private TableColumn<ChannelWrapper, Double> listOfWriteablesStart;

    @FXML
    private TableColumn<ChannelWrapper, Double> listOfWriteablesEnd;

    @FXML
    private TableColumn<ChannelWrapper, Integer> listOfWriteablesNpoints;

    @FXML
    private TableColumn<ChannelWrapper, Double> listOfWriteablesRef;

    @FXML
    private ListView<String> constraintsList;

    @FXML
    private Tab tabRun;

    @FXML
    private Button executeButton;

    @FXML
    private ProgressBar runProgressBar;

    @FXML
    private Text textFieldNumMeas;

    @FXML
    private Text textFieldTimeEstimate;

    @FXML
    private Button stopButton;

    @FXML
    private Button pauseButton;

    @FXML
    private Button restartButton;

    @FXML
    private Tab tabDisplay;

    @FXML
    private TableView<SimpleOption> expertOptions;

    @FXML
    private TableColumn<SimpleOption, Boolean> expertOptionDesc;

    @FXML
    private TableColumn<SimpleOption, Boolean> expertOptionSelected;

    @FXML
    private ListView<String> analyseList;

    @FXML
    private Plot plotArea;

    private ObservableList<String> measurements;

    private ObservableList<ChannelWrapper> pvScanList;

    private ObservableList<SimpleOption> runOptions;

    private static final Logger LOGGER = Logger.getLogger(FXMLController.class.getName());

    private MainFunctions functions;
    private static ScannerDocument document;

    @FXML
    private void handleScanAddPV(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/AddPV.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Add PV");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(document.getStage().getScene().getWindow());
            stage.setScene(new Scene(root));
            stage.show();
            LOGGER.log(Level.FINER, "Add PV Window opened");
        }
        catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error opening Add PV window", e);
        }
    }

    @FXML
    private void handleScanRemovePV(ActionEvent event) {
            pvTable.getSelectionModel().getSelectedItems().forEach(cW -> {
                    LOGGER.log(Level.INFO, "Parameter {0} removed from channel list", cW.getChannelName());
                    pvTable.getItems().remove(cW);
                    listOfWriteables.getItems().remove(cW);
                    document.setHasChanges(true);
            });
    }

    // Extend the the first dimension of the double array to length newLength
    private static double[][] extendArray(double[][] origArray, int newLength) {
        double[][] newArray = new double[newLength][origArray[0].length];
        for (int i=0;i<origArray.length;i++) {
            System.arraycopy(origArray[i], 0, newArray[i], 0, origArray[0].length);
        }
        return newArray;
    }

    @FXML
    void handlePreCalculate(ActionEvent event) {
        int nPoints = functions.calculateNumMeas();
        // Trigger update of Execute button
        functions.setCombosUpdated(true);
        int nDone = document.getNumCombosDone();
        textFieldNumMeas.setText("Number of measurement points: "+nPoints);
        if (nDone==0) {
            textFieldTimeEstimate.setText("This will take "+functions.getTimeString(nPoints));
        } else {
            textFieldTimeEstimate.setText((nPoints-nDone) + " remaining, this will take "
                    + functions.getTimeString(nPoints - nDone));
        }
    }

    @FXML
    private void handleRunExecute(ActionEvent event) {
        if (!document.sourceSetAndValid()) {
            Alert a = new Alert(AlertType.CONFIRMATION);
            a.setContentText("The data during this scan will not be saved, are you sure you want to continue?");
            Optional<ButtonType> result = a.showAndWait();
            if(!result.isPresent() || result.get() != ButtonType.OK)
                return;
        }
        functions.actionExecute();
        restartButton.setVisible(false);
        if (document.getNumCombosDone() == 0) {
            analyseList.getSelectionModel().clearSelection();
            analyseList.autosize();
        }
        plotArea.plotMeasurement();
    }

    @FXML
    void handleRunRestart(ActionEvent event) {
        // Similar to handleRunExecute, but we set combos done back to 0
        // We do not care about showing tabDisplay (it must be available at this point)
        document.setZeroCombosDone();
        functions.actionExecute();
        restartButton.setVisible(false);
        analyseList.getSelectionModel().clearSelection();
        analyseList.autosize();
        plotArea.plotMeasurement();
    }

    @FXML
    private void handleRunPause(ActionEvent event) {
        functions.triggerPause();
    }

    @FXML
    private void handleRunStop(ActionEvent event) {
        functions.triggerStop();
        // This does not seem to work properly.
        restartButton.setVisible(document.getNumCombosDone() != 0);
    }

    @FXML
    private void handleDeleteSelectedScans(ActionEvent event) {
        analyseList.getSelectionModel().getSelectedItems().forEach(item ->  deleteScan(item));
    }

    private void deleteScan(String setName) {
        Alert a = new Alert(AlertType.CONFIRMATION);
        a.setContentText("Are you sure you want to delete "+ setName + "?");
        Optional<ButtonType> result = a.showAndWait();
	if(result.isPresent() && result.get() == ButtonType.OK) {
            document.deleteSet(setName);
            //int i = analyseList.getItems().indexOf(setName);
            analyseList.getItems().remove(setName);
	}
    }

    @FXML
    private void handleInspectCommand(ActionEvent event) {
        // Check that the command written down is valid,
        // and if it is, allow to activate it
        String command = commandToExecute.getText().split("\\s+")[0];
        File filepath = new File(command);
        if (filepath.canExecute()) {
            commandActivated.setDisable(false);
            document.setCommandToExecute(commandToExecute.getText());
        } else {
            commandActivated.setDisable(true);
            document.setCommandToExecute("");
            String[] paths = System.getenv("PATH").split(":");
            for (String path: paths) {
                filepath = new File(path + "/" + command);
                if (filepath.canExecute()) {
                    commandActivated.setDisable(false);
                    break;
                }
            }

        }
        if (commandActivated.isDisabled())
            commandActivated.setSelected(false);

    }

    @FXML
    private void handleActivateCommand(ActionEvent event) {
        if (commandActivated.isSelected())
            document.setCommandActive(true);
        else
            document.setCommandActive(false);
    }

    private void dummyBugFunction() {
        // Needed due to problem with getSelectedItems()
    }

    @FXML
    private void analyseListMouseClicked(MouseEvent event) {
        plotArea.clear();
        // There is a problem in getSelectedItems() so need to call this twice..
        analyseList.getSelectionModel().getSelectedItems().forEach((meas) -> dummyBugFunction());
        analyseList.getSelectionModel().getSelectedItems().forEach((meas) -> {
            plotArea.plotMeasurement(meas);
                });
        document.setSelectedSets(analyseList.getSelectionModel().getSelectedItems());
    }

    @FXML
    private void handleMeasurementNameEdited(ListView.EditEvent<String> event) throws NoSuchFieldException {
        String oldName = analyseList.getItems().get(event.getIndex());
        functions.renameMeasurement(oldName, event.getNewValue());
        measurements.set(event.getIndex(), event.getNewValue());
    }


    @FXML
    void setDelayEdited(KeyEvent event) {
        if (!delayBetweenMeas.getText().equals("")) {
            LOGGER.log(Level.FINER, "Delay edited to {0}", delayBetweenMeas.getText());
            if((long)(Float.parseFloat(delayBetweenMeas.getText())*1000) != document.getDelayBetweenMeasurements())
                measDelaySetButton.setDisable(false);
            else
                measDelaySetButton.setDisable(true);
        }
    }

    @FXML
    void setMeasPerPointEdited(KeyEvent event) {
        if (!measPerPoint.getText().equals("")) {
            LOGGER.log(Level.FINER, "Measurements per point edited to {0}", measPerPoint.getText());
            if(Integer.parseInt(measPerPoint.getText())
                    != document.getNumberMeasurementsPerCombo())
                nMeasPerSettingSetButton.setDisable(false);
            else
                nMeasPerSettingSetButton.setDisable(true);
        }
    }


    @FXML
    void setMeasurementDelay(ActionEvent event) {
        document.setDelayBetweenMeasurements(Float.parseFloat(delayBetweenMeas.getText())*1000);
        LOGGER.log(Level.INFO, "Measurement delay set to {0} ms",
                   document.getDelayBetweenMeasurements());
        // If combos are already calculated we trigger a quick refresh of the calculation
        if (functions.getIsCombosUpdated())
            handlePreCalculate(event);
        measDelaySetButton.setDisable(true);
    }

    @FXML
    void setNumberOfMeasPerSetting(ActionEvent event) {
        document.setNumberMeasurementsPerCombo(Integer.parseInt(measPerPoint.getText()));
        LOGGER.log(Level.INFO, "Measurements per point set to {0}",
                   document.getNumberMeasurementsPerCombo());
        // If combos are already calculated we trigger a quick refresh of the calculation
        if (functions.getIsCombosUpdated())
            handlePreCalculate(event);
        nMeasPerSettingSetButton.setDisable(true);
    }

    void assertObject(Object obj, String name) {
        assert obj != null : "fx:id=\""+name+"\" was not injected: check your FXML file 'ScannerScene.fxml'.";
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initialize();
    }

    public void initialize() {

        assertObject(pvTable, "pvTable");
        assertObject(pvNameColumn, "pvNameColumn");
        assertObject(scanColumnSelect, "scanColumnSelect");
        assertObject(readColumnSelect, "readColumnSelect");

        assertObject(tabConfigure, "tabConfigure");
        assertObject(tabSelect, "tabSelect");
        assertObject(delayBetweenMeas, "delayBetweenMeas");
        assertObject(measPerPoint, "measPerPoint");
        assertObject(measDelaySetButton, "measDelaySetButton");
        assertObject(nMeasPerSettingSetButton, "nMeasPerSettingSetButton");
        assertObject(commandToExecute, "commandToExecute");
        assertObject(commandActivated, "commandActivated");
        assertObject(listOfWriteables, "listOfWriteables");
        assertObject(listOfWriteablesShortVar, "listOfWriteablesShortVar");
        assertObject(listOfWriteablesPV, "listOfWriteablesPV");
        assertObject(listOfWriteablesUnit, "listOfWriteablesUnit");
        assertObject(listOfWriteablesStart, "listOfWriteablesStart");
        assertObject(listOfWriteablesEnd, "listOfWriteablesEnd");
        assertObject(listOfWriteablesNpoints, "listOfWriteablesNpoints");
        assertObject(listOfWriteablesRef, "listOfWriteablesRef");
        assertObject(constraintsList, "constraintsList");

        assertObject(tabRun, "tabRun");
        assertObject(executeButton, "");
        assertObject(runProgressBar, "runProgressBar");
        assertObject(textFieldNumMeas, "textFieldNumMeas");
        assertObject(textFieldTimeEstimate, "textFieldTimeEstimate");
        assertObject(pauseButton, "pauseButton");
        assertObject(stopButton, "stopButton");
        assertObject(restartButton, "restartButton");
        assertObject(expertOptions, "expertOptions");
        assertObject(expertOptionDesc, "expertOptionDesc");
        assertObject(expertOptionSelected, "expertOptionSelected");

        assertObject(tabDisplay, "tabDisplay");
        assertObject(analyseList, "analyseList");
        assertObject(plotArea, "plotArea");



        initializeSelectionTables();
        initializeExpertOptions();

        // Initialize the configurations list

        // Initialize the list of measurements
        measurements = FXCollections.observableArrayList();
        analyseList.setItems(measurements);
        analyseList.setCellFactory(TextFieldListCell.forListView());
        analyseList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        listOfWriteables.setItems(pvScanList);
        listOfWriteablesPV.setCellValueFactory(new PropertyValueFactory<>("channelName"));
        listOfWriteablesShortVar.setCellValueFactory(new PropertyValueFactory<>("instance"));
        listOfWriteablesUnit.setCellValueFactory(new PropertyValueFactory<>("unit"));
        listOfWriteablesStart.setCellValueFactory(new PropertyValueFactory<>("start"));
        listOfWriteablesStart.setCellFactory(
                TextFieldTableCell.<ChannelWrapper, Double>forTableColumn(new DoubleStringConverter()));
        listOfWriteablesEnd.setCellValueFactory(new PropertyValueFactory<>("end"));
        listOfWriteablesEnd.setCellFactory(
                TextFieldTableCell.<ChannelWrapper, Double>forTableColumn(new DoubleStringConverter()));
        listOfWriteablesNpoints.setCellValueFactory(new PropertyValueFactory<>("numPoints"));
        listOfWriteablesNpoints.setCellFactory(
                TextFieldTableCell.<ChannelWrapper, Integer>forTableColumn(new IntegerStringConverter()));
        listOfWriteablesRef.setCellValueFactory(new PropertyValueFactory<>("ref"));
        listOfWriteablesRef.setCellFactory(
                TextFieldTableCell.<ChannelWrapper, Double>forTableColumn(new DoubleStringConverter()));

        functions = MainFunctions.getFunctions();

        // Initialize constraints
        constraintsList.setItems(document.getConstraints());
        constraintsList.setCellFactory(TextFieldListCell.forListView());
        constraintsList.setOnEditCommit((ListView.EditEvent<String> t) -> {
            constraintsList.getItems().set(t.getIndex(), t.getNewValue());
            document.setConstraint(t.getIndex(), t.getNewValue());
            functions.setCombosUpdated(false);
        });
        if (!document.constraintsAllowed) {
            constraintsList.setEditable(false);
            constraintsList.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton().equals(MouseButton.PRIMARY)){
                        if(event.getClickCount() == 2){
                            Alert a = new Alert(AlertType.WARNING);
                            a.setContentText("Constraints disabled, no available script engines");
                            a.show();
                            LOGGER.log(Level.FINER, "Constraints disabled, no available script engines");
                        }
                    }
                }
            });
        }

        // Only allow to push execute when the combo list is up to date..
        executeButton.setDisable(!functions.getIsCombosUpdated());
        functions.addIsCombosUpdatedListener(
                (ChangeListener) (ObservableValue observable, Object oldValue, Object newValue) -> {
            boolean executeAllowed = (Boolean) newValue;
            LOGGER.log(Level.FINEST, "Execute allowed: {0}", new Object[]{executeAllowed});
            executeButton.setDisable(!executeAllowed);
            });


        document.getScanNumberProperty().addListener((observable, oldValue, newValue) ->
                updateAnalysisList());

        // Similarly for stop and pause buttons..
        functions.addPauseListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
            if ((Boolean) newVal) {
                pauseButton.setText("Continue");
                restartButton.setVisible(false);
            } else {
                pauseButton.setText("Pause");
            }
        });

        functions.addStopListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> restartButton.setVisible((Boolean) newVal));

        functions.addRunOngoingListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
                tabConfigure.setDisable((Boolean) newVal);
                tabSelect.setDisable((Boolean) newVal);
        });

        // Deal with the progress of the run, activate to pause/stop buttons, plot last measurement etc..
        functions.addRunProgressListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
            if(functions.getRunProgress() < 1.0 && functions.isStopped() == false) {
                executeButton.setText("Running..");
                executeButton.setDisable(true);
                pauseButton.setVisible(true);
                stopButton.setVisible(true);
                runProgressBar.setProgress(functions.getRunProgress());
            } else {
                if(restartButton.isVisible())
                    executeButton.setText("Continue");
                else
                    executeButton.setText("Execute");
                executeButton.setDisable(false);
                pauseButton.setVisible(false);
                stopButton.setVisible(false);
                if (functions.isStopped() == false)
                    runProgressBar.setProgress(0.0);
            }
            plotArea.clear();
            plotArea.plotMeasurement();
        });


        functions.addIsCombosUpdatedListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
            if(!(Boolean)newVal) {
                textFieldNumMeas.setText("");
                textFieldTimeEstimate.setText("");
            }
        });


        document.addDelayBetweenMeasListener((observable, oldValue, newValue) -> {
                    Long delay = ((Number)newValue).longValue();
                    delayBetweenMeas.setText(String.valueOf(0.001 * delay));
                });
        delayBetweenMeas.setText(String.valueOf(0.001*document.getDelayBetweenMeasurements()));


        document.addNumberMeasPerCombosListener((observable, oldValue, newValue) -> {
                    measPerPoint.setText(newValue.toString());
                });
        measPerPoint.setText(String.valueOf(document.getNumberMeasurementsPerCombo()));


        document.addCurrentMeasLoadedListener(
                (ChangeListener) (ObservableValue o, Object oldValue, Object newValue) -> {
                    // When this property goes from true -> false it means we should prep the continuation
                    if ((Boolean) oldValue && !(Boolean) newValue) {
                        functions.triggerStop();
                        restartButton.setVisible(document.getNumCombosDone() != 0);
                        executeButton.setText("Continue");
                        // Should also trigger a plot here!
                    }
                });


        tabConfigure.setDisable(true);
        tabRun.setDisable(true);
        tabDisplay.setDisable(true);

        // -- configuration text fields --
        // Add ToolTips to both text fields
        Tooltip delayBetweenMeasTooltip = new Tooltip();
        delayBetweenMeasTooltip.setText("Set the delay between measurements");
        delayBetweenMeas.setTooltip(delayBetweenMeasTooltip);

        Tooltip measPerPointTooltip = new Tooltip();
        measPerPointTooltip.setText("Number of measurements at each combo");
        measPerPoint.setTooltip(measPerPointTooltip);
        // force the field to be a double only
        Pattern delayFieldPattern = Pattern.compile("\\d*|\\d+\\.\\d*");
        TextFormatter delayFieldFormatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return delayFieldPattern.matcher(change.getControlNewText()).matches() ? change : null;
        });
        delayBetweenMeas.setTextFormatter(delayFieldFormatter);
        // force the field to be numeric (integer) only
        Pattern measPerFieldPattern = Pattern.compile("\\d*");
        TextFormatter measPerFieldFormatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return measPerFieldPattern.matcher(change.getControlNewText()).matches() ? change : null;
        });
        measPerPoint.setTextFormatter(measPerFieldFormatter);
    }

    public static void setMainDocument(ScannerDocument scannerDocument) {
        document = scannerDocument;
    }


    private void initializeSelectionTables() {
        // Initialize the list of scan variables..
        pvScanList = FXCollections.observableArrayList();
        pvTable.setItems(document.getPVChannels());

        pvNameColumn.setCellValueFactory(new PropertyValueFactory<>("channelName"));

        readColumnSelect.setCellFactory(CheckBoxTableCell.forTableColumn((Integer param) -> {
            ChannelWrapper channel = document.getPVChannel(param);
            LOGGER.log(Level.FINEST, "Read selection trigger for {0}", channel);
            if (channel.getIsRead()) {
                functions.actionAddPv(channel, true, false);
                checkSufficientParams();
            } else {
                functions.actionRemovePV(channel, true, false);
                checkSufficientParams();
            }
            return document.getPVChannel(param).isReadProperty();
        }));
        // Trigger update whenever we select channel to be read (e.g. when loading from XML)
        readColumnSelect.setCellValueFactory(
                (CellDataFeatures<ChannelWrapper, Boolean> param) -> param.getValue().isReadProperty());

        scanColumnSelect.setCellFactory(column -> {
            return new CheckBoxTableCell<ChannelWrapper, Boolean>() {
                   @Override
                    public void updateItem(Boolean selected, boolean empty) {
                       super.updateItem(selected, empty);

                       ChannelWrapper channel = getTableRow().getItem();
                       setDisable(false); // it is required to fit default state
                       if (channel != null && !empty) {
                           if (channel.hasNode() && !channel.getIsScannable()) {
                               setDisable(true);
                               getTableRow().setTooltip(new Tooltip("Channel not settable"));
                           }
                           if (selected) {
                               LOGGER.log(Level.FINEST, "Enabled scan of {0}", channel.getChannel().channelName());
                               channel.setInstance();
                               if (functions.actionAddPv(channel, false, true)) {
                                   if (!pvScanList.contains(channel)) {
                                       pvScanList.add(channel);
                                       pvScanList.sort(ChannelWrapper::compareTo);
                                       functions.setCombosUpdated(false);
                                   }
                                   channel.isScannedProperty().set(true);
                               }
                           } else {
                               if(pvScanList.removeAll(channel)) {
                                   clearAllConstraints();
                                   functions.setCombosUpdated(false);
                                   LOGGER.log(Level.FINEST, "Disabled scan of {0}", channel.getChannelName());
                               }
                               channel.isScannedProperty().set(false);
                           }
                           checkSufficientParams();
                       }
                   }
            };
        });
        // Trigger update whenever we select channel to be scanned (e.g. when loading from XML)
        scanColumnSelect.setCellValueFactory(
                (CellDataFeatures<ChannelWrapper, Boolean> param) -> param.getValue().isScannedProperty());

        // Ideally should be able to select multiple channels to remove, but this does not seem to work.
        //readTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //scanTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


        final KeyCodeCombination keyCodeCopy = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
        pvTable.setOnKeyPressed(event -> {
            if (keyCodeCopy.match(event)) {
                LOGGER.log(Level.WARNING, "Tried to copy");
                for (ChannelWrapper cw: pvTable.getSelectionModel().getSelectedItems()) {
                    final ClipboardContent content = new ClipboardContent();
                    content.putString(cw.getChannelName());
                    Clipboard.getSystemClipboard().setContent(content);
                }
            }
        });

    }

    /**
     * Set extra run options
     */
    private void initializeExpertOptions() {
        // Initialize extra options for the run
        runOptions = FXCollections.observableArrayList();

        // Add an option to not return back to original settings after scan (default is to return back)
        SimpleOption returnHome = new SimpleOption("Return to initial setting (ref) after scan", true);

        returnHome.isSelectedProperty().bindBidirectional(document.getReturnHomeProperty());
        returnHome.isSelectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!Objects.equals(newValue, oldValue))
                document.setHasChanges(true);
        });
        runOptions.add(returnHome);

        // Include the initial settings of each PV in the scan (as first and last measurement point)
        SimpleOption includeInitial = new SimpleOption("Scan initial settings as first&last point", true);
        includeInitial.isSelectedProperty().bindBidirectional(document.getIncludeInitialSettingsProperty());
        includeInitial.isSelectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!Objects.equals(newValue, oldValue)) {
                functions.setCombosUpdated(false);
                document.setHasChanges(true);
            }
        });
        runOptions.add(includeInitial);

        // Reverse Scan (sawtooth scan)
        SimpleOption includeReverseScan = new SimpleOption("Append a reverse scan (sawtooth scan)", false);
        includeReverseScan.isSelectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!Objects.equals(newValue, oldValue)) {
                functions.setCombosUpdated(false);
                document.setHasChanges(true);
            }
        });
        includeReverseScan.isSelectedProperty().bindBidirectional(document.getIncludeReverseScanProperty());
        runOptions.add(includeReverseScan);

        // Reverse Scan (sawtooth scan)
        SimpleOption simple1B1 = new SimpleOption("Simple one-by-one scan", false);
        simple1B1.isSelectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!Objects.equals(newValue, oldValue)) {
                functions.setCombosUpdated(false);
                document.setHasChanges(true);
            }
        });
        simple1B1.isSelectedProperty().bindBidirectional(document.getSimple1B1Property());
        runOptions.add(simple1B1);


        expertOptions.setItems(runOptions);
        expertOptionDesc.setCellValueFactory(new PropertyValueFactory<>("description"));

        expertOptionSelected.setCellFactory(CheckBoxTableCell.forTableColumn((Integer param) -> {
            SimpleOption thisOption = expertOptions.getItems().get(param);
            LOGGER.log(Level.FINER, "Selected to flip \"{0}\", is now {1}",
                    new Object[]{thisOption.getDescription(), thisOption.isSelectedProperty().getValue()} );

            return thisOption.isSelectedProperty();
        }));
        expertOptionSelected.setCellValueFactory((CellDataFeatures<SimpleOption, Boolean> param) ->
                param.getValue().isSelectedProperty());
    }

    private void clearAllConstraints() {
        document.clearAllConstraints();
        constraintsList.setItems(document.getConstraints());
    }

    // Whenever there is an update to the list of finished measurements, this should be called
    private void updateAnalysisList() {
        // This is typically called by a thread, so use Platform.runLater in order to avoid IllegalStateException
        Platform.runLater(
            () -> {
                measurements.clear();
                document.getDataSets().entrySet().forEach(
                        dataSet -> measurements.add(dataSet.getKey()));
                if (measurements.size() >0 )
                    tabDisplay.setDisable(false);
                LOGGER.log(Level.FINER, "Analysis list updated");
            }
          );
    }

    /**
     * Check if we have selected enough parameters to do a scan
     *
     * @return true if we have at least one parameter listed to scan
     */
    private boolean checkSufficientParams() {
        boolean sufficient = !(pvScanList.isEmpty());
        if ( sufficient ) {
            tabConfigure.setDisable(false);
            tabRun.setDisable(false);
        } else {
            tabConfigure.setDisable(true);
            tabRun.setDisable(true);
        }
        return sufficient;
    }
}
