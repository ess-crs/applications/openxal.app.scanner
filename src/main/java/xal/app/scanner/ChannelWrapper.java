/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import xal.ca.Channel;
import xal.ca.ConnectionException;
import xal.ca.GetException;
import xal.ca.MonitorException;
import xal.ca.PutException;
import xal.smf.AcceleratorNode;

/**
 * A class that wraps a bit of additional functionality to the Channel class
 *
 *
 * @author yngvelevinsen
 */
public class ChannelWrapper {
    private final Channel mChannel;
    private final AcceleratorNode mNode;
    private final StringProperty mHandle;
    private final StringProperty mId;
    private final StringProperty mUnit;
    private final StringProperty mType;
    private final StringProperty mTypeHandle;
    private final SimpleDoubleProperty start;
    private final SimpleDoubleProperty end;
    private final SimpleDoubleProperty initialValue;
    private final SimpleIntegerProperty numPoints;
    private final SimpleBooleanProperty isScanned;
    private final SimpleBooleanProperty isRead;
    private final SimpleBooleanProperty isDouble;
    private final SimpleBooleanProperty isInt;
    private final SimpleStringProperty instance;
    private final int numberOfElements;
    private double[] scanPoints;
    private double defaultTimeOut;
    // This is probably not the best way to do this.
    private static int instanceCount = 0;

    private static final Logger LOGGER = Logger.getLogger(ChannelWrapper.class.getName());


    /**
     * Construct a new object from the Channel given, as well as connecting to it
     *
     * @param chan the channel to connect to
     */
    public ChannelWrapper(Channel chan) {
        this(chan, null, true);
    }

    /**
     * Construct a new object from the Channel given, as well as connecting to it
     *
     * @param chan the channel to connect to
     * @param tryConnect attempt to connect to channel
     */
    public ChannelWrapper(Channel chan, boolean tryConnect) {
        this(chan, null, tryConnect);
    }

    /**
     * Construct a new wrapper from the given node and handle.
     * Used by PVTree
     * Will not try to connect to channel (assumed orgnised outside)
     *
     * @param node The node handle belongs to
     * @param handle The name of the handle
     */
    public ChannelWrapper(AcceleratorNode node, String handle) {
        this(node.getChannel(handle), node, false);
    }


    /**
     * Construct a new object from the Channel given, as well as connecting to it
     *
     * @param chan the channel to connect to
     * @param node the node channel belongs to
     * @param tryConnect attempt to connect to channel
     */
    public ChannelWrapper(Channel chan, AcceleratorNode node, boolean tryConnect) {
        mChannel = chan;
        mNode = node;
        mHandle = new SimpleStringProperty(this, "");
        mTypeHandle = new SimpleStringProperty(this, "");
        defaultTimeOut = 5;
        if (mNode != null) {
            for (String handle : mNode.getHandles()) {
                if (mNode.findChannel(handle) != null) {
                    String chanId = mNode.getChannel(handle).getId();
                    if (chanId.equals(mChannel.getId())) {
                        mHandle.set(handle);
                    }
                }
            }
            mTypeHandle.set(mNode.getType() + ": " + mHandle.get());
        }
        if (tryConnect)
            mChannel.connectAndWait();
        isDouble = new SimpleBooleanProperty(true);
        isInt = new SimpleBooleanProperty(false);
        initialValue = new SimpleDoubleProperty(0.0);
        if (mChannel.isConnected()) {
            try {
                initialValue.set(mChannel.getRawValueRecord().doubleValue());
                isDouble.set(mChannel.getRawValueRecord().getType().equals(double.class));
                isInt.set(mChannel.getRawValueRecord().getType().equals(int.class));
            } catch (GetException ex) {
                LOGGER.log(Level.WARNING, null, ex);
            }
        }
        mId = new SimpleStringProperty(this, "");
        mType = new SimpleStringProperty(this, "");
        mUnit = new SimpleStringProperty(this, "");
        start = new SimpleDoubleProperty(initialValue.get()-1.0);
        end = new SimpleDoubleProperty(initialValue.get()+1.0);
        numPoints = new SimpleIntegerProperty(5);

        isScanned = new SimpleBooleanProperty(false);
        isRead = new SimpleBooleanProperty(false);
        instance = new SimpleStringProperty("x0");


        updateScanRange(start.get(),end.get());

        numPoints.addListener((observable, oldValue, newValue) -> updateScanRange(oldValue, newValue));
        start.addListener((observable, oldValue, newValue) -> updateScanRange(oldValue, newValue));
        end.addListener((observable, oldValue, newValue) -> updateScanRange(oldValue, newValue));
        mId.set(mChannel.getId());
        setAccesType();

        int nElements = 0;
        if (mChannel.isConnected()) {
           try {
               mUnit.set(mChannel.getUnits());
               nElements = mChannel.elementCount();

           } catch (ConnectionException | GetException ex) {
               LOGGER.log(Level.WARNING, null, ex);
           }
       }
       numberOfElements = nElements;
    }

    /**
     * Set value of the channel. If a node is known, and node has a readback channel,
     * then the readback will be monitored with a currently hard-coded 1% error.
     *
     * @param value The new value to set
     * @param timeout Timeout if readback changes too slowly
     * @throws ConnectionException If there was a connection issue
     * @throws PutException If the new value was not successfully set
     * @throws MonitorException If there was an issue with the channel monitor
     */
    public void putVal(double value, double timeout) throws PutException, MonitorException {
        if (getChannel().connectAndWait(defaultTimeOut)) {
            if (mNode != null) {
                double tolerance = (value == 0) ? 0.01 : Math.abs(value) * 0.01;

                if (isDouble.get()) {
                    if (!mNode.setValueAndVerify(mHandle.get(), value, tolerance, timeout)) {
                        getChannel().putVal(value);
                    }
                } else if (isInt.get()) {
                    if ((value % 1) != 0)
                        throw new PutException("Cannot convert "+value+" to integer");
                    int intVal = (int) value;
                    if (!mNode.setValueAndVerify(mHandle.get(), intVal, tolerance, timeout)) {
                        getChannel().putVal(intVal);
                    }
                } else {
                    throw new PutException("Unknown data type for channel");
                }


            } else {
                getChannel().putVal(value);
            }
            LOGGER.log(Level.INFO, "Put {0} to {1}", new Object[]{mId.getValue(), value});
        }

    }

    /**
     * Send a new value to the PV
     *
     * @param value The new value
     * @throws ConnectionException if there is a problem with the connection
     * @throws PutException if the new value cannot be set
     * @throws MonitorException if there is a problem with the PV monitor
     */
    public void putVal(double value) throws PutException, MonitorException {
        putVal(value, defaultTimeOut);
    }

    private void setAccesType() {
        if (mChannel.isConnected()) {
            try {
                if (mNode != null) {
                    if (mNode.isChannelSettable(mHandle.get()))
                        mType.set("rw");
                    else
                        mType.set("r");
                } else {
                    if (mChannel.readAccess() && mChannel.writeAccess()) {
                        if (mChannel.getRawValueRecord().getCount() == 1)
                            mType.set("rw");
                        else
                            mType.set("r");
                    } else if (mChannel.readAccess() ) {
                         mType.set("r");
                    } else if (mChannel.writeAccess() ) {
                        mType.set("w");
                    }
                }
            } catch (ConnectionException | GetException ex) {
                LOGGER.log(Level.WARNING, null, ex);
                mType.set("rw");
            }
        }
    }

    /**
     * @return The channel ID/name
     */
    public StringProperty idProperty() {
        return mId;
    }

    /**
     * @return Unique number that identifies the object
     */
    public SimpleStringProperty instanceProperty() {
        return instance;
    }

    /**
     * @return The unit for this channel
     */
    public StringProperty unitProperty() {
        return mUnit;
    }

    /**
     * @return Writeable or only readable
     */
    public StringProperty typeProperty() {
        return mType;
    }
    /**
     * Set to true if this channel is selected to be scanned (ie is active)
     *
     * @return true if channel will be scanned
     */
    public SimpleBooleanProperty isScannedProperty() {
        return isScanned;
    }
    /**
     * Set to true if this channel is selected to be read (ie is active)
     *
     * @return true if channel will be read
     */
    public SimpleBooleanProperty isReadProperty() {
        return isRead;
    }

    /**
     * @return Start value when scanned
     */
    public SimpleDoubleProperty startProperty() {
        return start;
    }

    /**
     * @return End value when scanned
     */
    public SimpleDoubleProperty endProperty() {
        return end;
    }

    /**
     * The initial/reference value of this channel
     * (i.e. the value it had when it was loaded into the application by default)
     * @return The initial/reference value of this channel
     */
    public SimpleDoubleProperty refProperty() {
        return initialValue;
    }

    /**
     * @return Number of points scanned
     */
    public SimpleIntegerProperty numPointsProperty() {
        return numPoints;
    }


    /**
     * @return The Channel object
     */
    public Channel getChannel() {
        return mChannel;
    }

    /**
     * @return The node this channel belongs to
     */
    public AcceleratorNode getAcceleratorNode() {
        return mNode;
    }

    /**
     * @return The ID of the node this channel belongs to
     */
    public String getAcceleratorNodeId() {
        if (mNode == null)
            return "";
        return mNode.getId();
    }

    /**
     * @return The handle for this channel, if it belongs to a node
     */
    public String getHandle() {
        return mHandle.get();
    }

    /**
     * @return The type of handle, if it belongs to a node
     */
    public String getTypeHandle() {
        return mTypeHandle.get();
    }

    /**
     *
     * @return true if this wrapper has an AcceleratorNode
     */
    public boolean hasNode() {
        return mNode != null;
    }

    /**
     * Name (ID) of channel
     */
    public String getChannelName() {
        return mId.getValue();
    }

    /**
     * True if channel will be scanned
     */
    public boolean getIsScanned() {
        return isScanned.get();
    }

    /**
     * @return True if channel belongs to a node and is settable
     */
    public boolean getIsScannable() {
        return mType.get().contains("w");
    }

    /**
     * True if channel will be read
     */
    public boolean getIsRead() {
        return isRead.get();
    }
    /**
     * If the channel only has one element, it is considered
     * a scalar
     * @return true if the channel is scalar
     */
    public boolean getIsScalar() {
        return numberOfElements == 1;
    }

    /**
     * Get the number of points when measured
     */
    public int getNumPoints() {
        return numPoints.get();
    }

    private void updateScanRange(Number oldValue, Number newValue) {
        if ( newValue == null || newValue.equals(oldValue))
            return;
        scanPoints = new double[numPoints.get()];
        for(int i = 0; i< numPoints.get(); i++)
            scanPoints[i] = start.get()+(end.get()-start.get())/(numPoints.get()-1)*i;
    }


    /**
     * Get the values to be scanned
     *
     *
     * @param reverse If true, return array in reverse order
     * @return An array of the values to scan
     */
    public double[] getScanPoints(boolean reverse) {
        if (scanPoints==null)
            updateScanRange(start.get(),end.get());
        if (reverse) {
            double[] ret = new double[scanPoints.length];
            for (int i=0; i<scanPoints.length; i++)
                ret[scanPoints.length - 1 - i] = scanPoints[i];
            return ret;
        }
        return scanPoints.clone();
    }

    public double[] getScanPoints() {
        return getScanPoints(false);
    }

    private static void increaseInstanceCount() {
        instanceCount++;
    }


    /**
     * Set the unique instance (integer) of this channel
     * Returns the string ('x'+integer)
     */
    public void setInstance() {
        if ("x0".equals(instance.get())) {
            increaseInstanceCount();
            instance.set("x"+instanceCount);
        }
    }

    /**
     * Force the unique instance name of this channel rather
     * than using the counter to obtain it
     *
     * @param shortName The name to be used
     */
    public void forceInstance(String shortName) {
        // Guessing that this counter will continue work,
        // but it is not guaranteed!
        increaseInstanceCount();
        instance.set(shortName);
        LOGGER.log(Level.FINEST, "Forced short name {0}, counter at {1}", new Object[]{shortName, instanceCount});
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.mId);
        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (!(other instanceof ChannelWrapper))
            return false;
        ChannelWrapper cw = (ChannelWrapper) other;
        return cw.getChannelName() == null ?
            this.getChannelName() == null : cw.getChannelName().equals(this.getChannelName());
    }

    /**
     * Compare the instance value to another ChannelWrapper object.
     * Used for sorting in the UI
     *
     * @param other ChannelWrapper
     * @return String comparison of the two instance values
     */
    public int compareTo(Object other) {
        if (other == null)
            return 1;
        return this.instance.getValue().compareTo(((ChannelWrapper) other).instance.getValue());

    }
}
