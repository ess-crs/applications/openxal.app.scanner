/* 
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * A simple (stupid you say?) class which holds a string and
 * a boolean to turn a setting on/off
 *
 * @author yngvelevinsen
 */
public class SimpleOption {
    private final SimpleBooleanProperty isSelected;
    private final SimpleStringProperty description;

    public SimpleOption(String descr, boolean initialSetting) {
        description = new SimpleStringProperty(descr);
        isSelected = new SimpleBooleanProperty(initialSetting);
    }

    /**
     * @propertyDescription If true, the option is.. active
     *
     * @return True if the setting is active/selected, false if deactivated
     */
    public SimpleBooleanProperty isSelectedProperty() {
        return isSelected;
    }

    /**
     *
     * @return The description for this option
     */
    public String getDescription() {
        return description.get();
    }
}
