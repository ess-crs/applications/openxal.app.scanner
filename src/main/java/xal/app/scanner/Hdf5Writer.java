/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import hdf.object.Attribute;
import hdf.object.Datatype;
import hdf.object.FileFormat;
import hdf.object.HObject;
import hdf.object.h5.H5Datatype;
import hdf.object.h5.H5File;
import hdf.object.h5.H5Group;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is an early template of what we need in terms of
 * static HDF5 writer.
 *
 * Should be refactored and put in a core i/o class
 *
 * @author yngvelevinsen
 */
public class Hdf5Writer  implements AutoCloseable {

    private final int chunks;
    private final int  gzip;
    private H5File h5File;

    private static final Logger LOGGER = Logger.getLogger(Hdf5Writer.class.getName());

    /**
     * Create a new writer object.
     * Need to call setSource() after to open the HDF5 file for writing.
     */
    public Hdf5Writer() {
        chunks = 1024;
        gzip = 0;
    }

    /**
     * Make sure HDF5 file is closed before instance is destroyed
     * (unsure if this is necessary tbh)
     */
    public void close() {
        closeSourceFile();
    }

    /**
     * Closes the HDF file access. Should also flush any remaining output.
     */
    public void closeSourceFile() {
        if (h5File != null)
            h5File.close();
        h5File = null;
    }


    /**
     * Set the path to the HDF file and open it
     *
     * @param filePath Path to HDF file
     * @throws Exception If there was a problem opening the file
     */
    public void setSource(String filePath) throws Exception {
        closeSourceFile();
        h5File = getH5file(filePath);
        LOGGER.log(Level.FINER, "Opened HDF5  {0} for writing", new Object[]{h5File.getAbsolutePath()});
    }

    public void writeArrayAsHDF5DataSet(String path, String tStamp, double[] data)
            throws Exception {
        // retrieve an instance of H5File.

        // Should not have chunks larger than the data itself
        int thisChunk = (data.length<chunks) ? data.length : chunks;

        H5Group group = getOrCreateGroup(h5File, path);

        LOGGER.log(Level.FINEST, "Writing dataset {0} to {1}", new Object[]{tStamp, group.getFullName()});
        H5Datatype dtype = new H5Datatype(Datatype.CLASS_FLOAT, Double.BYTES, Datatype.NATIVE, Datatype.NATIVE);

        // Deal with potentially trying to write same timestamp several times
        int i = 0;
        for (HObject object : group.getMemberList()) {
            if (object.getName().equals(tStamp) || object.getName().equals(tStamp+"-"+i))
                i+=1;
        }
        if (i>0)
            tStamp += "-" + i;

        h5File.createScalarDS(tStamp, group, dtype, new long[]{ data.length }, null, new long[]{thisChunk}, gzip, data);
    }

    public void writeDoubleAsHDF5DataSet(String path, String tStamp, double data)
            throws Exception {

        H5Group group = getOrCreateGroup(h5File, path);

        LOGGER.log(Level.FINEST, "Writing dataset {0} to {1}", new Object[]{tStamp, group.getFullName()});
        H5Datatype dtype = new H5Datatype(Datatype.CLASS_FLOAT, Double.BYTES, Datatype.NATIVE, Datatype.NATIVE);

        // Deal with potentially trying to write same timestamp several times
        int i = 0;
        for (HObject object : group.getMemberList()) {
            if (object.getName().equals(tStamp) || object.getName().equals(tStamp+"-"+i))
                i+=1;
        }
        if (i>0)
            tStamp+="-"+i;

        // data set needs to be a double array
        double[] dataset = {data};
        h5File.createScalarDS(tStamp, group, dtype, new long[]{1}, null, new long[]{1}, 0, dataset);
    }

    /**
     * Write a string attribute to a H5Object in the file
     *
     * @todo not possible to overwrite existing attribute atm
     * @param path Path to H5Object that should have the attribute
     * @param attributeName Name of attribute
     * @param attributeContent Content of attribute
     * @throws Exception If there is a problem creating the attribute
     */
    public void writeHDF5Attribute(String path, String attributeName, String attributeContent)
            throws Exception {

        H5Group group = getOrCreateGroup(h5File, path);

        LOGGER.log(Level.FINEST, "Writing attribute {0}: \"{1}\" to {2}", new Object[]{attributeName, attributeContent, group.getFullName()});
        H5Datatype dtype = new H5Datatype(Datatype.CLASS_STRING, attributeContent.length(), Datatype.NATIVE, Datatype.NATIVE);

        Attribute attr = new Attribute(group, attributeName, dtype, new long[]{1}, new String[]{attributeContent});
        group.writeMetadata(attr);
    }


    /**
     * If the name already exist in group, return it cast as a H5Group,
     * otherwise create it and return the object.
     *
     * @param name The name of the child group object
     * @param group The name of the parent group
     * @return The child group
     * @throws Exception If there was a problem creating the child group
     */
    private H5Group getOrCreateChildGroup(String name, H5Group group) throws Exception {
        for (HObject childGroup : group.getMemberList()) {
            if ( childGroup.getName().equals(name) )
                return (H5Group) childGroup;
        }

        return H5Group.create( name, group);
    }

    /**
     * If the name already exist in group, return it cast as a H5Group,
     * otherwise create it and return the object.
     *
     * @param h5File The name of the child group object
     * @param path The name of the parent group
     * @return The child group
     * @throws Exception If there was a problem creating a group
     */
    public H5Group getOrCreateGroup(H5File h5File, String path) throws Exception {

        // We only need to do this once at start of measurement!
        H5Group group = (H5Group) h5File.getRootObject();
        for ( String name : path.split("/")) {
            if (name.length()>0) {
                group = getOrCreateChildGroup(name, group);
            }
        }
        return group;
    }

    /**
     * Returns all groups one level below the group path given
     *
     * @param groupPath Path to group (e.g "/" to get top level)
     * @throws Exception If there was a problem creating a group
     * @return list of all groups one level below
     */
    public ArrayList<String> h5GetAllGroups(String groupPath) throws Exception {
        ArrayList<String> keysList = new ArrayList<String>();
        if (h5File != null) {
            H5Group group = getOrCreateGroup(h5File, groupPath);
            for (HObject subGroup : group.getMemberList()) {
                keysList.add(subGroup.getName());
            }
        }
        return keysList;
    }

    /**
     * retrieve an instance of H5File.
     * @param filePath Path to the file for this instance
     * @trows Exception If there is a problem opening the file
     */
    private static H5File getH5file(String filePath) throws Exception {

        FileFormat fileFormat = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);

        if (fileFormat == null) {
            LOGGER.log(Level.SEVERE, "Can't find HDF5 FileFormat. Check the java library path.");
            throw new RuntimeException();
        }

        File file = new File(getMeasurementsFileName(filePath));

        // create a new file with a given file name.
        H5File h5File = new H5File(file.getAbsolutePath(), FileFormat.FILE_CREATE_OPEN);

        h5File.open();

        return h5File;

    }

    /**
     * Get the file path for the measurements with correct ending for HDF5
     * corresponding to the main configuration file.
     *
     * @param source The file path for the configuration file
     * @return The string that should be used as file
     */
    static String getMeasurementsFileName(String source) {
        File file = (new File(source)).getAbsoluteFile();
        String folderPath = file.getParentFile().getAbsolutePath();
        String fileName = file.getName();
        String[] fileSplit = fileName.split("\\.");
        String fileEnd = fileSplit[fileSplit.length - 1];
        String fileAlmostEnd = fileSplit[fileSplit.length - 2];
        String fileBase;
        if (fileAlmostEnd.equals("scan"))
            fileBase = fileName.split("\\.scan\\."+fileEnd)[0];
        else
            fileBase = fileName.split("\\."+fileEnd)[0];
        if (fileEnd.equals("xml"))
            fileEnd = "h5";
        if (fileAlmostEnd.equals("scan"))
            fileEnd = ".measurements.scan." + fileEnd;
        else
            fileEnd = ".measurements." + fileEnd;
        if (! (fileEnd.endsWith("h5") || fileEnd.endsWith("hdf5")))
            throw new IllegalArgumentException("Wrong file type for HDF5: " + source);
        return folderPath + "/" + fileBase + fileEnd;
    }

    /**
     * Test HDF5 writing, return true if it works well
     * @return True if you are able to write HDF5 files
     */
    public static boolean h5LibsWorks() {
        FileFormat fileFormat = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);
        if (fileFormat == null)
            return false;
        return true;
    }
}
