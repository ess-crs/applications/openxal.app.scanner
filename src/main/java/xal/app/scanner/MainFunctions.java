/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import xal.ca.ChannelTimeRecord;
import xal.ca.GetException;
import xal.ca.MonitorException;
import xal.ca.PutException;

/**
 *
 * @todo It would be nice to refactor this and not do static stuff so much (ugh)
 *
 * @author yngvelevinsen
 */
class MainFunctions {

    private static ScannerDocument mainDocument;
    private static MainFunctions functions = null;

    // Holds the progress of the execution
    private static final SimpleDoubleProperty runProgress =  new SimpleDoubleProperty(-1.0);

    // True when a scan is ongoing
    private static final SimpleBooleanProperty isScanOngoingProperty = new SimpleBooleanProperty(false);

    // True if combos list is up to date
    private static final SimpleBooleanProperty isCombosUpdated =  new SimpleBooleanProperty(false);

    private static final SimpleBooleanProperty pauseTask = new SimpleBooleanProperty(false);
    private static final SimpleBooleanProperty stopTask = new SimpleBooleanProperty(false);

    private static final Logger LOGGER = Logger.getLogger(MainFunctions.class.getName());

    private static final InvalidationListener changeListener = observable -> {
            LOGGER.log(Level.FINEST, "ChangeListener triggered, combos must be recalculated");
            isCombosUpdated.set(false);
        };

    private MainFunctions() {

    }

    public static MainFunctions getFunctions() {
        if (functions == null)
            functions = new MainFunctions();
        return functions;
    }

    public static void setMainDocument(ScannerDocument scannerDocument) {
        mainDocument = scannerDocument;
    }

    public void addPauseListener(ChangeListener listener) {
        pauseTask.addListener(listener);
    }

    public void addStopListener(ChangeListener listener) {
        stopTask.addListener(listener);
    }

    public boolean isStopped() {
        return stopTask.get();
    }

    public void addRunProgressListener(ChangeListener listener) {
        runProgress.addListener(listener);
    }

    public void addRunOngoingListener(ChangeListener listener) {
        isScanOngoingProperty.addListener(listener);
    }

    public double getRunProgress() {
        return runProgress.get();
    }

    public void addIsCombosUpdatedListener(ChangeListener listener) {
        isCombosUpdated.addListener(listener);
    }

    public void setCombosUpdated(boolean updated) {
        isCombosUpdated.set(updated);
    }

    public boolean getIsCombosUpdated() {
        return isCombosUpdated.getValue();
    }

    /**
     * This should be called when the channel is selected in the GUI.
     *
     * @param cWrapper The channel to add
     * @param read Add the channel to readbacks
     * @param write Add the channel to writeables
     * @return true if the channel was added (ie was not already in list)
     */
    public boolean actionAddPv(ChannelWrapper cWrapper, Boolean read, Boolean write) {
        if (read)  {
            LOGGER.log(Level.FINER, "Added channel {0} to readable list", cWrapper);
            return true;
            }
        if (write) {
            LOGGER.log(Level.FINER, "Added channel {0} to writeable list", cWrapper);
            cWrapper.numPointsProperty().addListener(changeListener);
            cWrapper.startProperty().addListener(changeListener);
            cWrapper.endProperty().addListener(changeListener);
            cWrapper.refProperty().addListener(changeListener);
            // Need to make sure the initial PV reading is done again.
            mainDocument.unsetInitialCombo();
            return true;
        }
        return false;
    }

    /**
     * This should be called when the channel is unselected in the GUI.
     * It currently only have a function for writeable channels, but please
     * always call this when a channel is unselected (in case we need some logic
     * in the future).
     *
     * @param cWrapper The channel to remove
     * @param read Remove the channel from readbacks
     * @param write Remove the channel from writeables
     */
    public void actionRemovePV(ChannelWrapper cWrapper, Boolean read, Boolean write) {
        if (read)  {
            LOGGER.log(Level.FINER, "Removing channel {0} from readable list",cWrapper);
        }
        if (write) {
            LOGGER.log(Level.FINER, "Removing channel {0} from writeable list",cWrapper);
            cWrapper.numPointsProperty().removeListener(changeListener);
            cWrapper.startProperty().removeListener(changeListener);
            cWrapper.endProperty().removeListener(changeListener);
            cWrapper.refProperty().removeListener(changeListener);
            // Need to make sure the initial PV reading is done again.
            mainDocument.unsetInitialCombo();
        }
    }

    /*
     * This function updates combos with the correct combinations of settings
     * for each variable.
     *
     * Note that combos.get(0) always returns the INITIAL SETTINGS
     */
    public int calculateCombos() {
        mainDocument.calculateCombos();
        isCombosUpdated.set(true);
        return mainDocument.getNumberOfCombos();
    }

    /**
     * This calculates the total number of measurement points for this scan
     */
    public int calculateNumMeas() {
        if(!isCombosUpdated.getValue())
            calculateCombos();
        return 2 + (mainDocument.getNumberOfCombos() - 2) * mainDocument.getNumberMeasurementsPerCombo();
    }

    /*
     * Set a specific combo
     *
     */
    private void setCombo(double[] combo) {
        for (int i=0;i<combo.length;i++) {
            ChannelWrapper cw = mainDocument.getActivePVwriteback(i);
            double value = combo[i];
            try {
                cw.putVal(value, 20);
            } catch (PutException | MonitorException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
                threadedError("Could not set "+ cw.getChannelName()
                        + " to " + value, ex);
                return;
            }

        }
    }

    /**
     * This function is meant to be called from a thread. It opens an alert box
     * and prints an error message, then pauses the scan.
     *
     * @param message The text message to add in the alert box
     * @param exception The exception that occured
     */
    private void threadedError(String message, Exception exception) {
        Platform.runLater( () -> {
            Alert alertBox = new Alert(Alert.AlertType.ERROR);
            alertBox.setContentText( message +
                    "\n\n" + exception);
            alertBox.show();
            pauseTask.set(true);
        });

    }

    private ArrayList<ChannelTimeRecord> makeReadingOfScalars() {
        ArrayList<ChannelTimeRecord> readings = new ArrayList((int) mainDocument.getActivePVreadableScalars().count());
        for (int i=0;i<(int) mainDocument.getActivePVreadableScalars().count();i++) {
            try {
                // Here we do the actual reading of the PV..
                readings.add(mainDocument.getActivePVreadableScalar(i).getChannel().getTimeRecord());
                ChannelTimeRecord rec = readings.get(i);
            } catch (GetException ex) {
                threadedError("Could not read "+ mainDocument.getActivePVreadableScalar(i).getChannel().getId(), ex);
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        return readings;
    }

    private ArrayList<ChannelTimeRecord> makeReadingOfArrays() {
        ArrayList<ChannelTimeRecord> readings = new ArrayList((int) mainDocument.getActivePVreadableArrays().count());
        for (int i=0;i<(int) mainDocument.getActivePVreadableArrays().count();i++) {
            try {
                // Here we do the actual reading of the PV..
                LOGGER.log(Level.INFO, "Reading array from {0}",
                        mainDocument.getActivePVreadableArray(i).getChannel().channelName());
                readings.add(mainDocument.getActivePVreadableArray(i).getChannel().getTimeRecord());
            } catch (GetException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        return readings;
    }

    /**
     * Get a string representing the amount of time to do the scan
     *
     * @param nPoints Tme number of points in the scan
     * @return A string representation of the time needed
     */
    public String getTimeString(int nPoints) {
        int seconds = (int) (nPoints * mainDocument.getDelayBetweenMeasurements() / 1000);
        int hours = (seconds - seconds%3600) / 3600;
        int min = (seconds - seconds%60) / 60 - hours * 60;
        String time = ""+(seconds%60)+"s";
        if (seconds>59)
                time=""+min+"m, "+time;
        if (seconds>3599)
                time=""+hours+"h, "+time;
        return time;
    }

    public boolean isScanOngoing() {
        return isScanOngoingProperty.get();
    }

    public void actionExecute() {

        if (isScanOngoingProperty.get())
            return;
        isScanOngoingProperty.set(true);

        if (!isCombosUpdated.get()) calculateCombos();

        if (mainDocument.getNumCombosDone() == 0) {
            mainDocument.resetCurrentMeasArrays();
        }

        // Warning, this only saves if document is defined (user has saved before)
        MainFunctions.mainDocument.saveDocument();

        pauseTask.set(false);
        stopTask.set(false);

        Task runTask;
        runTask = new Task<Void>() {

            @Override
            public void run() {

                LOGGER.log(Level.INFO, "Starting a new scan {0}", mainDocument.getNumCombosDone());
                LOGGER.log(Level.INFO, "Number of scalars to read: {0}, arrays to read: {1}",
                        new Object[]{mainDocument.getActivePVreadableScalars().count(),
                            mainDocument.getActivePVreadableArrays().count()});
                mainDocument.setCurrentMeasurementKey();
                int numMeasDone=0;


                if (mainDocument.getNumCombosDone() > 0) {
                    LOGGER.log(Level.FINER, "Continuing old measurement, {0} combos already done",
                            mainDocument.getNumCombosDone());
                    numMeasDone = 1 + (mainDocument.getNumCombosDone() - 1) * mainDocument.getNumberMeasurementsPerCombo();
                }

                while (mainDocument.getNumCombosDone() < mainDocument.getNumberOfCombos()) {
                    numMeasDone = theRunLoop(numMeasDone);
                    updateProgress(mainDocument.getNumCombosDone(), mainDocument.getNumberOfCombos());

                    // if a stop is requested, break the task loop
                    if (stopTask.get()) {
                        isScanOngoingProperty.set(false);
                        break;
                    }
                }

                if (mainDocument.getReturnHome()) {
                    LOGGER.log(Level.FINER, "Returning to initial settings");
                    // Make sure we are back to initial settings!
                    setCombo(mainDocument.getInitialCombo());
                } else {
                    LOGGER.log(Level.INFO, "User has selected not to return to initial settings");
                }

                // If we finished the measurement, store the new data.
                if (mainDocument.getNumCombosDone() == mainDocument.getNumberOfCombos()) {
                    String setName = mainDocument.getCurrentMeasurementKey();
                    mainDocument.setDataSet(setName);
                    mainDocument.setPVreadbackData(setName);
                    mainDocument.setPVwriteData(setName);
                    mainDocument.setTimestamps(setName);
                    mainDocument.increaseScanNumber();
                    mainDocument.setZeroCombosDone();
                    // Note that this is only saved if document is defined
                    mainDocument.saveDocument();
                }
                isScanOngoingProperty.set(false);
            }

            @Override
            protected Void call() throws Exception {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
        runProgress.bind(runTask.progressProperty());
        new Thread(runTask).start();

    }

    /**
     * This it the main loop run by the thread in actionExecute
     * @param numMeasDone number of measurements done so far
     */
    private int theRunLoop(int numMeasDone) {
        Process commandProcess;
        String commandLine;
        int numMeasThisCombo;
        while (pauseTask.get()) {
            try {
                Thread.sleep(mainDocument.getDelayBetweenMeasurements());
            } catch (InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "Sleep thread interrupted", ex);
            }
        }

        setCombo(mainDocument.getCombo(mainDocument.getNumCombosDone()));

        // If we have a command to execute, do now:
        if (mainDocument.isCommandActive()) {
            try {
                commandProcess = Runtime.getRuntime().exec(mainDocument.getCommandToExecute());
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(commandProcess.getInputStream()));
                while ((commandLine = in.readLine()) != null) {
                    System.out.println(commandLine);
                }
                in.close();
                int exitValue = commandProcess.waitFor();
                switch (exitValue) {
                    case 0:
                        LOGGER.log(Level.FINER, "Command finished, continuing simulation");
                        break;
                    case 42:
                        LOGGER.log(Level.WARNING,
                                "Command exited with status value {0}, pausing simulation", exitValue);
                        // TODO Using runLater is hideous, but works so yay
                        Platform.runLater(MainFunctions::triggerPause);
                        break;
                    default:
                        LOGGER.log(Level.WARNING,
                                "Command exited with status value {0}, stopping simulation", exitValue);
                        // TODO Using runLater is hideous, but works so yay
                        Platform.runLater(MainFunctions::triggerStop);
                        break;
                }
            } catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }

        try {
            Thread.sleep(mainDocument.getDelayBetweenMeasurements());
        } catch (InterruptedException ex) {
            LOGGER.log(Level.SEVERE, "Sleep thread interrupted", ex);
        }

        // First and last measurement we only do once
        if (mainDocument.getNumCombosDone() != 0 &&
                mainDocument.getNumCombosDone() != mainDocument.getNumberOfCombos()-1)
            numMeasThisCombo = mainDocument.getNumberMeasurementsPerCombo();
        else
            numMeasThisCombo = 1;
        LOGGER.log(Level.FINEST, "Number of measurements with this combo {0}", numMeasThisCombo);

        for (int j=0;j<numMeasThisCombo;j++) {
            // First we read all PV's into TimeRecord objects:
            ArrayList<ChannelTimeRecord> scalarReadings = makeReadingOfScalars();
            ArrayList<ChannelTimeRecord> arrayReadings = makeReadingOfArrays();
 
            // First copy the set values for the channels that we write (not from EPICS)
            System.arraycopy(mainDocument.getCombo(mainDocument.getNumCombosDone()),
                    0,
                    mainDocument.getCurrentMeasurementArray()[numMeasDone],
                    0,
                    (int) mainDocument.getActivePvWritebacks().count());
            // Copy the read values into the same array (kept in memory and displayed in graphs)
            System.arraycopy(scalarReadings.stream().mapToDouble(m -> m.doubleValue()).toArray(),
                    0,
                    mainDocument.getCurrentMeasurementArray()[numMeasDone],
                    (int) mainDocument.getActivePvWritebacks().count(),
                    (int) mainDocument.getActivePVreadableScalars().count());
            // Copy the timestamps for each scalar reading
            System.arraycopy(scalarReadings.stream().map(m -> m.getTimestamp()).toArray(),
                    0,
                    mainDocument.getCurrentTimestampsArray()[numMeasDone],
                    0,
                    (int) mainDocument.getActivePVreadableScalars().count());

            if (mainDocument.getUsingHDF5()) {
                mainDocument.h5_writeCurrentSetValues();
                if (!arrayReadings.isEmpty())
                    mainDocument.h5_writeCurrentArrayData(arrayReadings);
                // Write the array data straight to file
                if (!scalarReadings.isEmpty())
                    mainDocument.h5_writeCurrentScalarData(scalarReadings);
            }

            mainDocument.saveCurrentMeas(numMeasDone);
            numMeasDone++;
        }
        mainDocument.increaseNumCombosDone();

        return numMeasDone;
    }

    /**
     * Change the name of a measurement
     *
     * @param oldName The current name of the measurement
     * @param newName The new name of the measurement
     */
    public void renameMeasurement(String oldName, String newName) throws NoSuchFieldException {
        if (! oldName.equals(newName)) {
            LOGGER.log(Level.INFO, "Renaming {0} to {1}", new Object[]{oldName, newName});
            // Renaming the group is hard, but we add/update "description" attribute instead
            // This means multiple measurements may have same description, which may cause issues
            mainDocument.setMeasurementName(oldName, newName);
        }
    }

    public static void triggerPause() {
        // flip the pause state true/false
        if (pauseTask.get()) {
            LOGGER.log(Level.INFO, "Continue triggered");
            pauseTask.set(false);
        } else {
            LOGGER.log(Level.INFO, "Pause triggered");
            pauseTask.set(true);
        }
    }

    public static void triggerStop() {
        LOGGER.log(Level.INFO, "Stop triggered");
        stopTask.set(true);
    }

}
