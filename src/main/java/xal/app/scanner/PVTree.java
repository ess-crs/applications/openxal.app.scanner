/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import java.io.IOException;
import java.util.ArrayList;
import javafx.fxml.FXMLLoader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuButton;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.TreeView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import org.apache.commons.lang3.StringUtils;
import xal.ca.BatchGetValueRequest;
import xal.ca.Channel;
import xal.ca.ChannelFactory;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;

public class PVTree extends SplitPane {

    private static final Comparator<? super AcceleratorNode> NODE_COMPARATOR_ALPHABETICALLY =
            (n1, n2) -> String.CASE_INSENSITIVE_ORDER.compare(StringUtils.defaultString(n1.getId()),
                    StringUtils.defaultString(n2.getId()));
    private static final Comparator<? super AcceleratorNode> NODE_COMPARATOR_BY_POSITION =
            (n1, n2) -> (int) ( n1.getPosition() - n2.getPosition() );
    private static final Comparator<? super AcceleratorSeq> SEQUENCE_COMPARATOR_ALPHABETICALLY =
            (s1, s2) -> String.CASE_INSENSITIVE_ORDER.compare(StringUtils.defaultString(s1.getId()),
                    StringUtils.defaultString(s2.getId()));
    private static final Comparator<? super AcceleratorSeq> SEQUENCE_COMPARATOR_BY_POSITION =
            (s1, s2) -> (int) ( s1.getPosition() - s2.getPosition() );


    private final Map<String, CheckMenuItem> typeMap = new TreeMap<>();
    private final ObservableList<ChannelWrapper> epicsChannels = FXCollections.observableArrayList();

    private static final Logger LOGGER = Logger.getLogger(PVTree.class.getName());

    @FXML // fx:id="elementSearch"
    private TextField elementSearch;

    @FXML // fx:id="elementSearchIcon"
    private StackPane elementSearchIcon;

    @FXML // fx:id="elementSearchMenuButton"
    private MenuButton elementSearchMenuButton;

    @FXML // fx:id="elementSortAlphabetically"
    private RadioMenuItem elementSortAlphabetically;

    @FXML // fx:id="elementTree"
    private TreeView<AcceleratorNode> elementTree;

    @FXML // fx:id="manualTextField"
    private TextField manualTextField;
    private Channel manualChannel;

    @FXML // fx:id="manualChannelFoundStatus"
    private Label manualChannelFoundStatus;

    @FXML // fx:id="pvCheckManualButton"
    private Button pvCheckManualButton;

    @FXML // fx:id="epicsTable"
    private TableView<ChannelWrapper> epicsTable;

    @FXML // fx:id="epicsNameColumn"
    private TableColumn<ChannelWrapper, String> epicsNameColumn;

    @FXML // fx:id="epicsTypeColumn"
    private TableColumn<ChannelWrapper, String> epicsTypeColumn;

    @FXML // fx:id="pvAddButton"
    private Button pvAddButton;

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert elementSearch != null : "fx:id=\"elementSearch\" was not injected: check your FXML file 'PVTree.fxml'.";
        assert elementTree != null : "fx:id=\"elementTree\" was not injected: check your FXML file 'PVTree.fxml'.";
        assert pvAddButton != null : "fx:id=\"pvAddButton\" was not injected: check your FXML file 'PVTree.fxml'.";

        Model.getInstance().getAccelerator().getRoot().getAllNodes()
                .stream().map(AcceleratorNode::getType).distinct()
                .sorted().forEachOrdered(t -> addTypeMenuItem(t));

        elementTree.setCellFactory(p -> new NodeTreeCell());
        elementTree.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        elementTree.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> updateEPICSTable(oldValue, newValue));

        epicsTable.itemsProperty().setValue(epicsChannels);
        epicsNameColumn.setCellValueFactory(new PropertyValueFactory<>("typeHandle"));
        epicsTypeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        epicsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


        manualTextField.textProperty().addListener((obs, oldText, newText) -> {
            enteringManualPV(oldText, newText);
            // ...
        });

        elementSearch.textProperty().addListener(( observable, oldValue, newValue ) -> {

            if ( newValue.isEmpty() ) {
                elementSearchIcon.getStyleClass().clear();
                elementSearchIcon.getStyleClass().add("search-magnifying-glass"); //NOI18N
            } else {
                elementSearchIcon.getStyleClass().clear();
                elementSearchIcon.getStyleClass().add("search-clear"); //NOI18N
            }

            updateTree(newValue);

        });

        updateTree(elementSearch.getText());
        LOGGER.log(Level.FINER, "PV Tree Widget initialized");
    }

    private void addOneChannel(Channel channel, AcceleratorNode node) {
        ChannelWrapper newChannelWrapper = new ChannelWrapper(channel, node, true);
        ScannerDocument.getDocument().addPVChannel(newChannelWrapper);
        ScannerDocument.getDocument().setHasChanges(true);
    }

    private void addOneChannel(Channel channel) {
        addOneChannel(channel, null);
    }

    @FXML
    void addSelectedPV(ActionEvent event) {
        LOGGER.log(Level.INFO, "Adding selected channels");
        int count = ScannerDocument.getDocument().getNumPVChannels();

        if (epicsTable.getSelectionModel().getSelectedItems().size()>0) {
            epicsTable.getSelectionModel().getSelectedItems().forEach((ChannelWrapper cw) -> {
                elementTree.getSelectionModel().getSelectedItems().forEach( value -> {
                    AcceleratorNode node = value.getValue();
                    if (node.getType().equals(cw.getAcceleratorNode().getType())) {
                        Channel chan = node.findChannel(cw.getHandle());
                        if (chan!=null) {
                            chan.connectAndWait();
                            if (chan.isConnected()) {
                                addOneChannel(chan, node);
                            } else {
                                LOGGER.log(Level.WARNING, "Could not connect to {0}, not adding", chan.getId());
                            }
                        }
                    }
                    });
                });
        } else if (manualChannel.isConnected()) {
            addOneChannel(manualChannel);
        } else {
            LOGGER.log(Level.WARNING, "Nothing to add");
        }
        LOGGER.log(Level.INFO, "Added {0} channels.", ScannerDocument.getDocument().getNumPVChannels() - count);
        LOGGER.log(Level.FINEST, "Channels: {0}", ScannerDocument.getDocument().getPVChannels() );
    }


    @FXML
    void elementClearSearchField(MouseEvent event) {
        elementSearch.clear();
        LOGGER.log(Level.FINER, "Search field cleared");
    }

    @FXML
    void elementSortChanged(ActionEvent event) {
        throw new UnsupportedOperationException("Changing sorting type not implemented yet)");
    }

    public PVTree() {
        FXMLLoader fxmlLoader = new FXMLLoader(

        getClass().getResource("/widgets/PVTree.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
           fxmlLoader.load();
        } catch (IOException exception) {
           throw new RuntimeException(exception);
        }
    }

    @FXML
    void checkManualPV(ActionEvent event) {
        // This function is called when button "Check" is clicked under the manual PV entry
        // the Add button should be enabled if the PV is found
        // Ideally we want an area where information about the button is added (units, current value, r or r/w etc)
        LOGGER.log(Level.FINER, "Trying to connect to channel {0}", manualTextField.getText());
        ChannelFactory cFact = ChannelFactory.defaultFactory();
        manualChannel = cFact.getChannel(manualTextField.getText());
        try {
            manualChannel.connectAndWait();
            if (manualChannel.isConnected()) {
                pvAddButton.setDisable(false);
                LOGGER.log(Level.FINER, "Connected to channel {0}", manualTextField.getText());
                manualChannelFoundStatus.setText("Channel found, click Add button");
            } else {
                LOGGER.log(Level.WARNING, "Did not manage to connect to channel {0}", manualTextField.getText());
                manualChannelFoundStatus.setText("Channel NOT found, maybe wrong case?");
            }

        } catch (Exception exception) {
            LOGGER.log(Level.WARNING, "Exception raised when trying to connect to channel {0}",
                    manualTextField.getText());
        }
    }

    void enteringManualPV(String oldText, String newText) {
        // This function is called when something is written in the text field for manual entry.
        // Any selection in the element tree should be deselected, and the "Add" button should be disabled
        if (newText.length()>0)
        {
            epicsTable.getSelectionModel().clearSelection();
            epicsChannels.clear();
            elementTree.getSelectionModel().clearSelection();
        }
        pvAddButton.setDisable(true);

    }

    private void addTypeMenuItem( String type ) {
        final CheckMenuItem menuItem = new CheckMenuItem(type);

        if (!Arrays.asList("marker").stream().anyMatch(str -> str.trim().equals(type)))
            menuItem.setSelected(true);
        else
            menuItem.setSelected(false);

        if (!Arrays.asList("marker", "sequence").stream().anyMatch(str -> str.trim().equals(type)))
            elementSearchMenuButton.getItems().add(menuItem);

        typeMap.put(type, menuItem);
    }

    /**
     * Check if the list of EPICS parameters already contain this typeHandle
     * @param typeHandle The handle to compare against
     * @return true if typeHandle is found in the table
     */
    private boolean epicsTableContains(String typeHandle) {
        return epicsTable.getItems().stream().anyMatch((item) -> (item.getTypeHandle().equals(typeHandle)));
    }

    /**
     * Adds the Handle to the list if it is not already there..
     *
     * @param cw
     */
    private void addHandleToList(ChannelWrapper cw) {
        if (! epicsTableContains(cw.getTypeHandle())) {
            epicsChannels.add(cw);
        }
    }

    private void updateEPICSTable( TreeItem<AcceleratorNode> oldValue, TreeItem<AcceleratorNode> newValue) {


        if (oldValue == newValue)
            return;
        if ( newValue != null ) {
            // Clear manual text entry (we add EITHER manual entry OR selected entries)
            manualTextField.clear();
            manualChannelFoundStatus.setText("");

            LOGGER.log(Level.FINEST, "Epics table updating...");
            epicsChannels.clear();

            // First we connect all channels selected
            ArrayList<Channel> channels = new ArrayList<>();
            elementTree.getSelectionModel().getSelectedItems().forEach(item -> {
                if (item != null)
                    channels.addAll(item.getValue().getAllChannels());
            });
            final BatchGetValueRequest request = new BatchGetValueRequest( channels );
            LOGGER.log(Level.FINER, "Connecting {0} channels...", channels.size());
            if (!request.submitAndWait(2))
                LOGGER.log(Level.INFO, "Connection failed for {0} channels...", channels.size() - request.getRecordCount());

            // Then we initiate the updated list
            elementTree.getSelectionModel().getSelectedItems().forEach(value -> {
                if (value != null) {
                    AcceleratorNode node = value.getValue();
                    node.getHandles()
                        .stream()
                        .filter(h -> node.findChannel(h) != null)
                        .sorted(( h1, h2 ) -> String.CASE_INSENSITIVE_ORDER.compare(h1, h2))
                        .forEach(h -> addHandleToList(new ChannelWrapper(node, h)));
                }
            });



            //epicsChannels.parallelStream().forEach(cw -> cw.updateAfterConnected());

            LOGGER.log(Level.FINEST, "Epics table updated");
        }
    }

    private void updateTree( String searchPattern ) {

        Accelerator accelerator = Model.getInstance().getAccelerator();
        TreeItem<AcceleratorNode> rootNode = new TreeItem<>(accelerator.getRoot(),
                new ImageView(getClass().getResource("/icons/A.png").toExternalForm()));

        rootNode.setExpanded(true);
        populateTreeWithSequences(rootNode, accelerator, searchPattern);

        elementTree.setRoot(rootNode);

        LOGGER.log(Level.FINEST, "PV Tree widget updated");

    }

    /**
     * @param node          The node to be tested for search pattern and type.
     * @param searchPattern The search string.
     * @return {@code true} if the node ID contains the search string and its
     *         type is one of the admitted ones.
     */
    private boolean filterNode( AcceleratorNode node, String searchPattern ) {

        boolean idSearch = StringUtils.isEmpty(searchPattern) ||
                StringUtils.contains(StringUtils.upperCase(node.getId()), StringUtils.upperCase(searchPattern));
        boolean typeSearch = typeMap.get(node.getType()).isSelected();

        return typeSearch && idSearch;

    }

    /**
     * Populate the {@code parent} node.
     *
     * @param parent        The node to be populated.
     * @param sequence      The accelerator sequence corresponding to the given
     *                      node to be populated.
     * @param searchPattern The search string. If not {@code null} and not empty,
     *                      will filter out nodes whose ID doesn't contain the
     *                      search string.
     */
    private void populateTreeWithSequences( TreeItem<AcceleratorNode> parent,
                                            AcceleratorSeq sequence,
                                            String searchPattern ) {

        final Comparator<? super AcceleratorNode> nComparator =
                elementSortAlphabetically.isSelected() ? NODE_COMPARATOR_ALPHABETICALLY : NODE_COMPARATOR_BY_POSITION;
        final Comparator<? super AcceleratorSeq> sComparator =
                elementSortAlphabetically.isSelected() ?
                SEQUENCE_COMPARATOR_ALPHABETICALLY : SEQUENCE_COMPARATOR_BY_POSITION;

        sequence.getSequences().stream().sorted(sComparator).forEach(s -> {
                TreeItem<AcceleratorNode> sequenceNode = new TreeItem<>(s,
                        new ImageView(getClass().getResource("/icons/S.png").toExternalForm()));

                populateTreeWithSequences(sequenceNode, s, searchPattern);

                s.getNodes().stream().sorted(nComparator).filter(n -> filterNode(n, searchPattern)).forEach(n -> {
                    sequenceNode.getChildren().add(new TreeItem<>(n,
                            new ImageView(getClass().getResource("/icons/N.png").toExternalForm())));
                    sequenceNode.setExpanded(!StringUtils.isEmpty(searchPattern));
                });

                if ( !sequenceNode.isLeaf() || filterNode(s, searchPattern) ) {

                        parent.getChildren().add(sequenceNode);

                    if ( sequenceNode.isExpanded() ) {
                        parent.setExpanded(true);
                    }

                }
        });
    }
}
