/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner;

import eu.ess.xaos.ui.plot.LineChartFX;
import eu.ess.xaos.ui.plot.NumberAxis;
import eu.ess.xaos.ui.plot.plugins.Plugins;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.StackPane;
import xal.ca.Channel;

/**
 * FXML Controller class
 *
 * @author yngvelevinsen
 */
public class Plot extends SplitPane implements Initializable {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="pvReadbacksPane"
    private StackPane pvReadbacksPane;

    @FXML // fx:id="pvWriteablesPane"
    private StackPane pvWriteablesPane;

    private LineChartFX<Number, Number> chartRB;
    private LineChartFX<Number, Number> chartW;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        assert pvReadbacksPane != null : "fx:id=\"pvReadbacksGraph\" was not injected: check 'Plot.fxml'.";
        assert pvWriteablesPane != null : "fx:id=\"pvWriteablesGraph\" was not injected: check 'Plot.fxml'.";

        ValueAxis<Number> xAxis = new NumberAxis();
        ValueAxis<Number> yRBAxis = new NumberAxis();
        ValueAxis<Number> yWAxis = new NumberAxis();

        xAxis.setLabel("Measurement");
        yRBAxis.setLabel("Value");
        yWAxis.setLabel("Value");

        xAxis.setAnimated(false);
        yRBAxis.setAnimated(false);
        yWAxis.setAnimated(false);

        chartRB = new LineChartFX<>(xAxis, yRBAxis);
        chartRB.setAnimated(false);
        chartRB.setShowMarkers(true);
        chartRB.setOnMouseClicked(event -> chartRB.requestFocus());
        chartRB.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));

        chartW = new LineChartFX<>(xAxis, yWAxis);
        chartW.setAnimated(false);
        chartRB.setShowMarkers(true);
        chartW.setOnMouseClicked(event -> chartW.requestFocus());
        chartW.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));

        pvReadbacksPane.getChildren().add(chartRB);
        pvWriteablesPane.getChildren().add(chartW);
    }

    public Plot() {
        FXMLLoader fxmlLoader = new FXMLLoader(

        getClass().getResource("/fxml/Plot.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
           fxmlLoader.load();
        } catch (IOException exception) {
           throw new RuntimeException(exception);
        }
    }

    public void clear() {
        chartRB.getData().clear();
        chartW.getData().clear();
    }

    // Plot measurement of name measName
    public void plotMeasurement(String measName) {
        ScannerDocument doc = ScannerDocument.getDocument();
        String setKey = doc.getSetKey(measName);
        double [][] measurement = doc.getDataSet(setKey);
        List<Channel> pvR = doc.getPVrbScalarData(setKey);
        List<ChannelWrapper> pvW = doc.getPVWriteData(setKey);
        plotMeasurement(measurement, pvW, pvR);
    }

    // Plot the current (ongoing) measurement
    public void plotMeasurement() {
        List<Channel> _pvR = new ArrayList<>();
        List<ChannelWrapper> _pvW = new ArrayList<>();
        ScannerDocument.getDocument().getPVChannels().forEach(cWrap -> {
            if (cWrap.getIsRead()) _pvR.add(cWrap.getChannel());
            if (cWrap.getIsScanned()) _pvW.add(cWrap);
        });
        plotMeasurement(ScannerDocument.getDocument().getCurrentMeasurementArray(),_pvW,_pvR);
    }

    // Manually provide list of data and list of channels for the plot
    private void plotMeasurement(double [][] measurement, List<ChannelWrapper> pvWriteables, List<Channel> pvReadbacks) {
        if (measurement != null) {
            ObservableList<XYChart.Series<Number, Number>> chartDataW = FXCollections.observableArrayList();

            for (int i=0;i<pvWriteables.size();i++) {
                XYChart.Series<Number, Number> series = new XYChart.Series();
                for (int j=series.getData().size();j<measurement.length;j++) {
                    series.getData().add( new XYChart.Data(j, measurement[j][i]) );
                }
                series.setName(pvWriteables.get(i).getChannelName());
                chartDataW.add(series);
            }
            chartW.setData(chartDataW);

            ObservableList<XYChart.Series<Number, Number>> chartDataRB = FXCollections.observableArrayList();
            for (int i=pvWriteables.size();i<measurement[0].length;i++) {
                XYChart.Series<Number, Number> series = new XYChart.Series();
                for (int j=0;j<measurement.length;j++) {
                    series.getData().add( new XYChart.Data(j, measurement[j][i]) );
                }
                series.setName(pvReadbacks.get(i-pvWriteables.size()).getId());
                chartDataRB.add(series);
            }
            chartRB.setData(chartDataRB);
        }
    }

}
