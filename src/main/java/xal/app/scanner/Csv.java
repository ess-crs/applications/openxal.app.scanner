/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xal.app.scanner;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import xal.ca.Timestamp;

/**
 *
 * @author yngvelevinsen
 */
public class Csv {

    private File outFile;

    public Csv(File fPath) {
        outFile = fPath;
        // empty for now
    }

    public void setFilePath(String newPath) {
        outFile = new File(newPath);
    }

    public void setFilePath(File newPath) {
        outFile = newPath;
    }

    public String getFilePath() {
        return outFile.toString();
    }

    private String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }
    private String convertToCSV(String[] data) {
        return Stream.of(data).map(this::escapeSpecialCharacters).collect(Collectors.joining(","));
    }
    private void givenDataArray_whenConvertToCSV_thenOutputCreated(List<String[]> dataLines) throws IOException {
        try (PrintWriter pw = new PrintWriter(outFile)) {
            dataLines.stream()
              .map(this::convertToCSV)
              .forEach(pw::println);
        }
    }

    private List<String[]> dataToString(Timestamp[] tStamps, double[][] data) {
        List<String[]> table = new ArrayList<>();
        String[] row;
        for (int i=0; i< data.length; i++) {
            row = new String[data[i].length + 1];
            row[0] = tStamps[i].toString();
            for(int j=0; j< data[i].length; j++) {
                row[j + 1] = ""+data[i][j];
            }
            table.add(row);
        }
        return table;
    }

    public void exportToCSV(List<String> headers, Timestamp[] tStamps, double[][] data) {
        List<String[]> table = new ArrayList<>();
        int nchannels = data[0].length;
        String[] h = new String[nchannels + 1];
        h[0] = "Timestamp";
        System.arraycopy(headers.toArray(new String[0]), 0, h, 1, nchannels);
        table.add(h);
        table.addAll(dataToString(tStamps, data));
        try {
            givenDataArray_whenConvertToCSV_thenOutputCreated(table);
        } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
