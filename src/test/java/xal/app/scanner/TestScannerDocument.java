package xal.app.scanner;

import org.junit.experimental.runners.Enclosed;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class TestScannerDocument {

    private ScannerDocument document;

    @BeforeEach
    public void setUp() {
        document = ScannerDocument.initScannerDocument(null);
        File filePath = new File(getClass().getResource("test.scan.xml").getFile());
        document.setSource(filePath);
        document.loadDocument(true);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void usingHDF5() {
        assertFalse(document.getUsingHDF5());
    }
    @Test
    public void loadEmptyDocument() throws IOException {
        //File filePath = new File("empty.scan.xml");
        File filePath = new File(getClass().getResource("empty.scan.xml").getFile());
        document.setSource(filePath);
        document.loadDocument(true);
    }


    @Test
    public void countCombos() {
        document.clearAllConstraints();
        document.calculateCombos();
        int ncombos = (3*4*2) * 2 - 1 + 2; // *2-1 for rev scan, +2 for init settings
        assertEquals(ncombos, document.getNumberOfCombos());
        assertEquals(ncombos, document.calculateCombos());

        document.setIncludeInitialSettings(false);
        ncombos -= 2;
        assertEquals(ncombos, document.calculateCombos());

        document.setIncludeReverseScan(false);
        ncombos = (ncombos + 1)/2;
        assertEquals(ncombos, document.calculateCombos());

        document.setNumberMeasurementsPerCombo(3);
        assertEquals(3, document.getNumberMeasurementsPerCombo());

        document.getSimple1B1Property().set(true);
        document.setIncludeReverseScan(false);

        assertEquals(0, document.getNumCombosDone());
        document.increaseNumCombosDone();
        assertEquals(1, document.getNumCombosDone());
        document.setZeroCombosDone();
        assertEquals(0, document.getNumCombosDone());
    }

    @Test
    public void constraints() {
        int i = 0;
        String constraint = "x1 > 0";
        assertEquals("x1 > 0.001", document.getConstraints().get(i));
        document.setConstraint(i, constraint);
        assertEquals(constraint, document.getConstraints().get(i));
        assertEquals(15, document.calculateCombos());
        document.clearAllConstraints();
        assertEquals("", document.getConstraints().get(i));

    }

    @Test
    public void countPVs() {
        int nPV = document.getNumPVChannels();
        assertEquals(7, nPV);
    }

    @Test
    public void checkDelay() {
        assertEquals(300, document.getDelayBetweenMeasurements());
        document.setDelayBetweenMeasurements(800);
        assertEquals(800, document.getDelayBetweenMeasurements());
    }

    @Test
    public void getDataSets() {
        double[] ds = document.getDataSetForPV("2021-12-28_15:22:17", "MEBT-010:PwrC-PSCH-001:Fld-S");
        assertEquals(-0.003, ds[0], 1e-8);

    }
    @Test
    public void simple1B1() {
        document.clearAllConstraints();
        assertFalse(document.getSimple1B1Property().get());
        document.getSimple1B1Property().set(true);
        assertEquals(17, document.calculateCombos());
        document.setIncludeReverseScan(false);
        assertEquals(11, document.calculateCombos());
        document.setIncludeInitialSettings(false);
        assertEquals(9, document.calculateCombos());
    }
}
