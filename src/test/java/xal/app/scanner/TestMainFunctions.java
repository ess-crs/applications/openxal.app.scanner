package xal.app.scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;


import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;

import java.io.File;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TestMainFunctions extends FxRobot {

    private ScannerDocument document;
    private MainFunctions functions;

    @BeforeEach
    void setUp() {
        document = ScannerDocument.initScannerDocument(null);
        File filePath = new File(getClass().getResource("test.scan.xml").getFile());
        document.setSource(filePath);
        document.loadDocument(true);
        String editPath = filePath.getAbsolutePath().replace("test.scan.xml", "test.edited.scan.xml");
        document.setSource(new File(editPath));

        functions = MainFunctions.getFunctions();
        functions.setMainDocument(document);
    }

    @Test
    void calculateCombos() {
        assertEquals(15, functions.calculateNumMeas());
    }

    @Test
    void actionExecute() throws InterruptedException, TimeoutException {
        document.setDelayBetweenMeasurements(100);
        document.setIncludeReverseScan(false);
        document.setIncludeInitialSettings(false);
        functions.calculateCombos();
        FxToolkit.registerPrimaryStage();
        functions.actionExecute();
        functions.actionExecute(); // should do nothing since scan already ongoing..
        Thread.sleep(200);
        functions.triggerPause(); // pause
        Thread.sleep(200);
        functions.triggerPause(); // continue
        Thread.sleep(1000);
        functions.triggerStop();
        Thread.sleep(500); // wait for thread to close
    }

    @Test
    void actionAddPv() {
        Accelerator acc = Model.getInstance().getAccelerator();
        AcceleratorNode q = acc.getAllNodesOfType("Q").get(0);
        ChannelWrapper cW = new ChannelWrapper(q, "fieldSet");
        functions.actionAddPv(cW, true, false);
        functions.actionRemovePV(cW, true, false);
        cW = new ChannelWrapper(q, "fieldRB");
        functions.actionAddPv(cW, false, true);
        functions.actionRemovePV(cW, false, true);
    }

    @Test
    void timeCalculations() {
        assertEquals(300, document.getDelayBetweenMeasurements());
        assertEquals("30s", functions.getTimeString(100));
        document.setDelayBetweenMeasurements(800);
        assertEquals("2h, 16m, 8s", functions.getTimeString(10210));
    }

    @Test
    void measurementManipulations() throws NoSuchFieldException {
        functions.renameMeasurement("2021-12-28_15:22:17", "New Name"); // a valid change
        assertThrows(NoSuchFieldException.class, () -> {
            functions.renameMeasurement("notCorrectName", "another new name"); // should fail
        });
    }
}