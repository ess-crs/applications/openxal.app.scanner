package xal.app.scanner;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;
import static xal.app.scanner.Hdf5Writer.getMeasurementsFileName;

/**
 * Note, HDF needs to be configured on your system for these tests to pass
 */
class TestHdf5Writer {

    private Hdf5Writer h5Writer;
    private String filePath;

    @BeforeEach
    void setUp() throws Exception {
        filePath = "test.scan.xml";
        // We need to delete for now because we cannot overwrite an attribute
        // so test will fail if we run twice
        File fileObj = new File("test.measurements.scan.h5");
        fileObj.delete();
        h5Writer = new Hdf5Writer();
        h5Writer.setSource(filePath);
    }

    @AfterEach
    void tearDown() {
        h5Writer.close();
    }

    @Test
    @RepeatedTest(value = 1)
    @EnabledIfEnvironmentVariable(named = "H5TESTS", matches = "true")
    void writeArrayAsHDF5DataSet() throws Exception {
        double[] data = {1, 2, 3, 4};
        h5Writer.writeArrayAsHDF5DataSet("/arrayTest", "1641040662", data);
    }

    @Test
    @RepeatedTest(value = 1)
    @EnabledIfEnvironmentVariable(named = "H5TESTS", matches = "true")
    void writeDoubleAsHDF5DataSet() throws Exception {
        h5Writer.writeDoubleAsHDF5DataSet("/doubleTest", "1641040662", 1337.42);
    }

    @Test
    @EnabledIfEnvironmentVariable(named = "H5TESTS", matches = "true")
    void writeHDF5Attribute() throws Exception {
        h5Writer.writeHDF5Attribute("/attrTest", "testAttr", "Test Content");
    }

    @Test
    @EnabledIfEnvironmentVariable(named = "H5TESTS", matches = "true")
    void h5LibsWorks() {
        assertTrue(Hdf5Writer.h5LibsWorks());
    }

    @Test
    @EnabledIfEnvironmentVariable(named = "H5TESTS", matches = "true")
    void testGetMeasurementsFileName() {
        assertTrue(getMeasurementsFileName("test.scan.xml").contains("test.measurements.scan.h5"));
        assertTrue(getMeasurementsFileName("test.xml").contains("test.measurements.h5"));
        assertTrue(getMeasurementsFileName("test.scan.h5").contains("test.measurements.scan.h5"));
        assertTrue(getMeasurementsFileName("test.h5").contains("test.measurements.h5"));
        assertTrue(getMeasurementsFileName("test.scan.hdf5").contains("test.measurements.scan.hdf5"));
        assertThrows(IllegalArgumentException.class, () -> {
            getMeasurementsFileName("test.scan.txt");
        });
    }
}