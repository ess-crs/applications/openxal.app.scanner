package xal.app.scanner;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestSimpleOption {

    @Test
    void simpleOption() {
        SimpleOption option = new SimpleOption("Some descr", false);
        assertFalse(option.isSelectedProperty().get());
        assertEquals("Some descr", option.getDescription());
        option.isSelectedProperty().set(true);
        assertTrue(option.isSelectedProperty().get());
    }
}