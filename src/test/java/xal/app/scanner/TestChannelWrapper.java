/*
 * Copyright (C) 2021 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.scanner.test;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import xal.app.scanner.ChannelWrapper;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import xal.app.scanner.Model;
import xal.ca.Channel;
import xal.ca.ChannelFactory;
import xal.ca.ConnectionException;
import xal.ca.GetException;
import xal.ca.MonitorException;
import xal.ca.PutException;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.data.XMLDataManager;
import xal.smf.impl.BPM;
import xal.smf.impl.Quadrupole;

/**
 * TestChannelWrapper.java
 *
 * @author Yngve Levinsen
 * @since  Jan 10, 2018
 */
public class TestChannelWrapper {


    /*
     * Global Constants
     */

    /** EPICS ID of test device */
    public static final String  MAGNET_FLDSET_HANDLE = "fieldSet";
    public static final String  MAGNET_FLDSET_CHANNEL = "ST3-VC-PS:FldSet";
    public static final String  BPM_AVG_HANDLE = "yAvg";
    public static final String  BPM_AVG_CHANNEL = "BPM2:YAvg";
    public static final long WAIT_TIME = 50; // ms
    public static final long THREAD_WAIT_TIME = 10; // ms

    final static private ChannelFactory CHANNEL_SERVER_FACTORY = ChannelFactory.newServerFactory();

    private static ChannelWrapper CHAN_READ_WRAP;
    private static ChannelWrapper CHAN_SET_WRAP;
    private static SimpleVAThread vaThread;
    private Quadrupole quad;
    private BPM bpm;
    /*
     * Global Variables
     */

    @BeforeEach
    public void setUpBeforeClass() throws GetException, ConnectionException {

        // Create the Channel to connect to the PV.
        bpm = new BPM("BPM2", CHANNEL_SERVER_FACTORY);
        bpm.channelSuite().putChannel(BPM_AVG_HANDLE, BPM_AVG_CHANNEL, false);

        quad = new Quadrupole("Q1", CHANNEL_SERVER_FACTORY);
        quad.channelSuite().putChannel(MAGNET_FLDSET_HANDLE, MAGNET_FLDSET_CHANNEL, true);

        Channel CHAN_READ = bpm.getChannel(BPM_AVG_HANDLE);
        Channel CHAN_SET  = quad.getChannel(MAGNET_FLDSET_HANDLE);

        CHAN_SET.connectAndWait(1);
        CHAN_READ.connectAndWait(1);

        vaThread = new SimpleVAThread(THREAD_WAIT_TIME, CHAN_READ, CHAN_SET);
        vaThread.start();

        CHAN_READ_WRAP = new ChannelWrapper(CHAN_READ);
        CHAN_SET_WRAP = new ChannelWrapper(CHAN_SET, true);

        CHAN_SET_WRAP.setInstance();
        CHAN_SET_WRAP.isScannedProperty().set(true);
        CHAN_READ_WRAP.isReadProperty().set(true);

    }

    @AfterEach
    public void tearDownAfterClass() {
        CHAN_READ_WRAP = null;
        CHAN_SET_WRAP = null;
        vaThread.interrupt();
        vaThread = null;
    }

    /**
     * Test method for {@link openxal.apps.scanner}.
     */
    @Test
    public void channelWorks() {

        assertTrue( "Set channel is not valid", CHAN_SET_WRAP.getChannel().isValid());
        assertTrue("Read channel is not valid", CHAN_READ_WRAP.getChannel().isValid());
        assertTrue( "Set channel is not connected", CHAN_SET_WRAP.getChannel().isConnected());
        assertTrue("Read channel is not connected", CHAN_READ_WRAP.getChannel().isConnected());
        String instance0 = CHAN_SET_WRAP.instanceProperty().get();
        CHAN_SET_WRAP.setInstance(); // verify doing this twice has no effect
        assertEquals(instance0, CHAN_SET_WRAP.instanceProperty().get());
    }

    /**
     * Test method for {@link openxal.apps.scanner}.
     */
    @Test
    public void channelReadWrite() throws ConnectionException, GetException, InterruptedException, PutException, MonitorException {
        CHAN_SET_WRAP.getChannel().connectAndWait(0.5);
        CHAN_READ_WRAP.getChannel().connectAndWait(0.5);

        CHAN_SET_WRAP.putVal(0.5); // read should end up 2.0375
        TimeUnit.MILLISECONDS.sleep(WAIT_TIME);
        assertTrue("Read channel value is "+CHAN_READ_WRAP.getChannel().getValDbl()+", should be 2.0375", CHAN_READ_WRAP.getChannel().getValDbl()>2.0374);
        assertTrue("Read channel value is "+CHAN_READ_WRAP.getChannel().getValDbl()+", should be 2.0375", CHAN_READ_WRAP.getChannel().getValDbl()<2.0376);
    }

    @Test
    public void noSuchChannel() {
        Channel notExisting = ChannelFactory.defaultFactory().getChannel("No-such-PV");
        ChannelWrapper chanWrap = new ChannelWrapper(notExisting, true);
        assertFalse(chanWrap.getChannel().isConnected());
    }

    @Test
    public void modelAndAccNode() throws PutException, ConnectionException, MonitorException {
        Model modelInstance = Model.getInstance();
        Accelerator acc = modelInstance.getAccelerator();
        // This assumes there is at least one bpm in the acc tested..
        AcceleratorNode bpmNode = acc.getAllNodesOfType("BPM").get(0);
        ChannelWrapper cWrap = new ChannelWrapper(bpmNode, BPM_AVG_HANDLE);
        assertTrue(cWrap.getAcceleratorNode().isKindOf("BPM"));
        ChannelWrapper cWrapQuad = new ChannelWrapper(quad, MAGNET_FLDSET_HANDLE);
        assertEquals("rw", cWrapQuad.typeProperty().get());
        cWrapQuad.putVal(1.5);
        ChannelWrapper cWrapBpm = new ChannelWrapper(bpm, BPM_AVG_HANDLE);
        assertEquals("r", cWrapBpm.typeProperty().get());
    }

    @Test
    public void equality() {
        assertFalse(CHAN_READ_WRAP == CHAN_SET_WRAP);
        assertFalse(CHAN_READ_WRAP.equals(CHAN_SET_WRAP));
        assertFalse(CHAN_READ_WRAP.equals(null));
        assertFalse(CHAN_READ_WRAP.equals("another object"));
        assertTrue(CHAN_READ_WRAP.equals(CHAN_READ_WRAP));

        assertTrue( 0 > CHAN_READ_WRAP.compareTo(CHAN_SET_WRAP));
        assertTrue( 0 < CHAN_SET_WRAP.compareTo(CHAN_READ_WRAP));
        assertTrue( 0 < CHAN_READ_WRAP.compareTo(null));
        assertEquals(0, CHAN_SET_WRAP.compareTo(CHAN_SET_WRAP));
    }

    @Test
    public void properties() {
        assertEquals("BPM2:YAvg", CHAN_READ_WRAP.idProperty().get());
        assertEquals("x0", CHAN_READ_WRAP.instanceProperty().get());
        assertEquals("", CHAN_READ_WRAP.unitProperty().get());
        assertTrue(CHAN_SET_WRAP.getIsScalar());
        assertFalse(CHAN_SET_WRAP.getIsRead());
        assertTrue(CHAN_SET_WRAP.getIsScanned());
        assertTrue(CHAN_SET_WRAP.getIsScannable());
        assertFalse(CHAN_SET_WRAP.hasNode());
        assertEquals("rw", CHAN_SET_WRAP.typeProperty().get());
    }

}

/**
 * A very simple VA taking in two channels.
 * The read channel is set to 1.35*x^2-1.2*x+2.3 where x is assumed the set channel
 * Both channels assumed doubles.
 *
 * @author yngvelevinsen
 */
class SimpleVAThread extends Thread {
    long ms;
    double setVal;
    Channel readChan;
    Channel setChan;
    SimpleVAThread(long msec, Channel readChannel, Channel setChannel) throws ConnectionException, GetException {
        this.ms = msec;

        this.readChan = readChannel;
        this.setChan = setChannel;
        this.readChan.connectAndWait();
        this.setChan.connectAndWait();

        this.setVal = setChannel.getValDbl();
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                // do a simple edit of the readable channel
                TimeUnit.MILLISECONDS.sleep(this.ms);

                if (setVal != setChan.getValDbl()) {
                    setVal = setChan.getValDbl();
                    readChan.putVal(1.35*setVal*setVal-1.2*setVal+2.3);
                }
            } catch (GetException | PutException ex) {
                Logger.getLogger(SimpleVAThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

    }
}
