package xal.app.scanner;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import xal.ca.Timestamp;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class TestCsv {
    private Csv converter;

    @BeforeEach
    void setUp() {
        converter = new Csv(new File("./out.csv"));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void setFilePath() {
        String currentPath = converter.getFilePath();
        // test providing a file object
        converter.setFilePath(new File("./someOtherName.csv"));
        assertEquals("./someOtherName.csv", converter.getFilePath());
        // test providing a string object
        converter.setFilePath(currentPath);
        assertEquals(currentPath, converter.getFilePath());
    }

    @Test
    void exportToCSV() {
        ArrayList<String> headers = new ArrayList<>();
        headers.add("Column 1");
        headers.add("Column 2");
        BigDecimal ts1 = new BigDecimal(1640995262);
        BigDecimal ts2 = new BigDecimal(1641040662);
        Timestamp[] tStamps = {new Timestamp(ts1), new Timestamp(ts2)};
        double[][] data = {{1, 2}, {3, 4}};

        converter.exportToCSV(headers, tStamps, data);

        // TODO maybe add some validation here..


    }
}